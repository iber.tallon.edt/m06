#! /usr/bin/python3
#-*- coding: utf-8-*-
#
# Python documentation: https://docs.python.org/es/3/library/subprocess.html
# argparse tutorial: https://docs.python.org/es/3/howto/argparse.html
#  
# iber@edt ASIX M06 Curs 2023-2024
# 
# exemple-fork.py
# -----------------------------

#============= Importació de llibreries ==============
import sys, os
print("Hola, començament del programa principal")
print("PID pare: ", os.getpid())

pid=os.fork() # Es vifurca a partir del retorn del fork
if pid != 0: # Pregunta si es el pare, si es el pare fa els seguent, si es fill, fa el else
    #os.wait() # Espera a que finalitzi els processos fills.
    print("Programa pare: ",os.getpid(), pid) # Mostra el pid del pare
else:
    print("Programa fill: ", os.getpid(), pid) # Mostra el pid del fill
    while True:
        pass
print("Hasta luego Lucas!")
sys.exit(0)