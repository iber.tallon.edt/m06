#! /usr/bin/python3
#-*- coding: utf-8-*-
#
# Python documentation: https://docs.python.org/es/3/library/subprocess.html
# argparse tutorial: https://docs.python.org/es/3/howto/argparse.html
#  
# iber@edt ASIX M06 Curs 2023-2024
# 
# -----------------------------

#============= Importació de llibreries ==============
import sys, os, signal



#============= Programa ==============
print("Hola, començament del programa principal")
print("PID pare: ", os.getpid())

pid=os.fork() # Es vifurca a partir del retorn del fork
if pid != 0: # Pregunta si es el pare, si es el pare fa els seguent, si es fill, fa el else
    #os.wait() # Espera a que finalitzi els processos fills.
    print("Programa pare: ",os.getpid(), pid) # Mostra el pid del pare
    sys.exit(0)

# programa fill
#os.execv("/usr/bin/ls", ["/usr/bin/ls", "-ls", "/"])# Substitueix el propi process per un nou process (Tindrà el mateix PID). Es posen com a un array 
# de python.
#os.execl("/usr/bin/ls", "/usr/bin/ls", "-la", "/")# Es posen un a un separats per coma.
#os.execlp("ls", "ls", "-la", "/")
#os.execvp("uname", ["uname", "-a"])
#os.execv("/bin/bash", ["/bin/bash", "show.sh"])
os.execle("/bin/bash", "/bin/bash", "show.sh", {"nom":"joan", "edat":"25"})

print("Hasta luego Lucas")
sys.exit(0)