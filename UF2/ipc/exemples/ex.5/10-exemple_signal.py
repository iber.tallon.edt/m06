#! /usr/bin/python3
#-*- coding: utf-8-*-
#
# Python documentation: https://docs.python.org/es/3/library/subprocess.html
# argparse tutorial: https://docs.python.org/es/3/howto/argparse.html
#  
# iber@edt ASIX M06 Curs 2023-2024
# 
# -----------------------------

#============= Importació de llibreries ==============
import sys, os, signal

#Definir la funció "myhandler". 
def myhandler(signum, frame):
    print("Signal handler with signal: ", signum)
    num = int(sys.argv[1]) + 60
    print("La alarma a pujat 60 segons!")
    signal.alarm(num)

def nodeath(signum, frame):
     print("Signal handler with signal: ", signum)
     print("Tururu! millor tú")

def alarm(signum, frame):
    print("Signal handler with signal: ", signum)
    print("El temps a vençut!")
    sys.exit(0)

# Assignar un handler(encarregat de gestionar) al senyal. 
signal.signal(signal.SIGUSR1,myhandler) # Signal 10(SIGUSR1)
signal.signal(signal.SIGUSR2,nodeath) # Signal 12(SIGUSR1)
signal.signal(signal.SIGALRM,alarm) # Signal 14(SIGALRM)
signal.signal(signal.SIGTERM,signal.SIG_IGN) # Signal 15(SIGTERM)
signal.signal(signal.SIGINT,signal.SIG_IGN) # Signal 2(SIGIGN)

signal.alarm(int(sys.argv[1]))
print(os.getpid())

# Bucle infinit. 
while True:
    pass

sys.exit(0)