#! /usr/bin/python3
#-*- coding: utf-8-*-
#
# Python documentation: https://docs.python.org/3/library/argparse.html
# argparse tutorial: https://docs.python.org/es/3/howto/argparse.html
#  
# iber@edt ASIX M06 Curs 2023-2024
# -----------------------------
import argparse

#============= Definició d'arguments (description, prog, epilog) ==============

parser = argparse.ArgumentParser(   
   description="programa d'exemple d'arguments",\
   prog="02-arguments.py",\
   epilog="hasta luego lucas!")

# Definició d'un argument a processar.
parser.add_argument("-n","--nom", type=str,\
   help="nom usuari") 

parser.add_argument("-e","--edat", type=int,\
   dest="userEdat", help="edat a processar",\
   metavar="edat")

args = parser.parse_args()
print(args)


#============= Programa ==============

print(args.userEdat, args.nom)
exit(0)
