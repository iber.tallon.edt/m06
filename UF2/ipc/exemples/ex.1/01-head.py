#! /usr/bin/python3
#-*- coding: utf-8-*-
#
# iber@edt ASIX M06 Curs 2023-2024
# -----------------------------
import sys
max=5
fileIn=sys.stdin
if len(sys.argv) == 2:
  fileIn=open(sys.argv[1], "r")
count=0
for line in fileIn:
    count+=1
    print(line,end="")
    if count==max:
        break
fileIn.close()
exit(0)


