#! /usr/bin/python3
#-*- coding: utf-8-*-
#
# Python documentation: https://docs.python.org/es/3/library/subprocess.html
# argparse tutorial: https://docs.python.org/es/3/howto/argparse.html
#  
# iber@edt ASIX M06 Curs 2023-2024
# 
# programa.py 
# -----------------------------

#============= Importació de llibreries ==============

import sys, argparse
from subprocess import Popen, PIPE

parser = argparse.ArgumentParser(\
    description="Consulta a la base de dades entrada per argument al programa")
parser.add_argument("sqlStatement", type=str,\
    help="Consulta a la base de dades postgres")
args=parser.parse_args()


#============= Programa ==============

command = "PGPASSWORD=passwd  psql -qtA -F',' -h localhost  -U postgres training"

# Popen = contructor del túnel que fa connexió entre python i l'ordre que es trobi dins de la variable.
pipeData = Popen(command, shell=True, bufsize=0,\
        universal_newlines=True, 
        stdin=PIPE, stdout=PIPE,stderr=PIPE)
pipeData.stdin.write(args.sqlStatement + '\n\q\n')



# pipeData.stdout = pipeData.stdout.read()
for line in pipeData.stdout:
    print(line,end="")
exit(0)
