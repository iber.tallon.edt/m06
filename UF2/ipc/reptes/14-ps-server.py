#! /usr/bin/python3
#-*- coding: utf-8-*-
#
# Python documentation: https://docs.python.org/es/3/library/subprocess.html
# argparse tutorial: https://docs.python.org/es/3/howto/argparse.html
#  
# iber@edt ASIX M06 Curs 2023-2024
# 
# -----------------------------

#============= Importació de llibreries ==============
import sys,socket,argparse, os, signal
from subprocess import Popen, PIPE
parser = argparse.ArgumentParser(description="""ps Server """)

#============= Definició d'arguments (description, prog, epilog) ==============
parser.add_argument("-p","--port",type=int, default=50001)
args=parser.parse_args()
HOST = ''
PORT = args.port
llistaPears = []

#============= Definir Funcions ==============

def usr1(signum, frame):
 print("Signal handler with signal:", signum)
 print(llistaPears)
 sys.exit(0)


def usr2(signum, frame):
 print("Signal handler with signal:", signum)
 print(llistaPears)
 sys.exit(0)


def term(signum,frame):
 print("Signal handler called with signal:", signum)
 print(len(llistaPears))
 sys.exit(0)

pid=os.fork()
if pid != 0: 
    print("Programa pare: ",os.getpid(), pid) # Mostra el pid del pare
    print("Llançat el procés")
    sys.exit(0)


#============= Programa ==============
signal.signal(signal.SIGUSR1,usr1) # Signal 10(SIGUSR1)
signal.signal(signal.SIGUSR2,usr2) # Signal 12(SIGUSR1)
signal.signal(signal.SIGTERM,term) # Signal 15(SIGTERM)

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((PORT))
s.listen(1)
while True:
  conn, addr = s.accept()
  print("Connected by", addr)
  llistaPears.append(addr) # Afegir llista clients
  fileName ="/tmp/%s-%s-%s.log" % (addr[0],addr[1]) # Genera el nom del fitxer
  fileLog = open(fileName, "w")
  while True:
    ps = conn.recv(1024)
    fileLog.write(ps)
    if not ps: break
    fileLog.close()
    s.close()
  
