#! /usr/bin/python3
#-*- coding: utf-8-*-
#
# Python documentation: https://docs.python.org/es/3/library/subprocess.html
# argparse tutorial: https://docs.python.org/es/3/howto/argparse.html
#  
# iber@edt ASIX M06 Curs 2023-2024
# 
# -----------------------------

#============= Importació de llibreries ==============
import sys, os, signal, argparse

#============= Variables globals ==============
up=0
down=0


parser = argparse.ArgumentParser(description="Gestinar alarma")
parser.add_argument("segons", type=int, help="segons")
args=parser.parse_args()
print(args)
#============= Definir Funcions ==============

def usr1(signum, frame):
 global up
 print("Signal handler with signal:", signum)
 actual=signal.alarm(0)
 signal.alarm(actual+60)
 up+=1


def usr2(signum, frame):
 global down
 print("Signal handler with signal:", signum)
 actual=signal.alarm(0)
 if actual > 60:
   signal.alarm(actual-60)
 else:
   print("ignored, not enough time ",actual)
   signal.alarm(actual) 
 down+=1


def alarm(signum,frame):
 print("Signal handler called with signal:", signum)
 print("Finalitzant... up: %d down:%d" % (up, down))
 sys.exit(0)


def term(signum,frame):
 print("Signal handler called with signal:", signum)
 falten=signal.alarm(0)
 signal.alarm(falten)
 print("Falten actualment %d segons" % (falten))


def hup(signum,frame):
 print("Signal handler called with signal:", signum)
 print("Restoring value: ", args.segons)
 signal.alarm(args.segons)



    

#============= Programa ==============
# Assignar un handler(encarregat de gestionar) al senyal. 
signal.signal(signal.SIGUSR1,usr1) # Signal 10(SIGUSR1)
signal.signal(signal.SIGUSR2,usr2) # Signal 12(SIGUSR1)
signal.signal(signal.SIGALRM,alarm) # Signal 14(SIGALRM)
signal.signal(signal.SIGTERM,term) # Signal 15(SIGTERM)
signal.signal(signal.SIGHUP,hup) # Signal 1(SIGHUP)
signal.signal(signal.SIGINT,signal.SIG_IGN) # Signal 2(SIGIGN)

signal.alarm(args.segons)
print(os.getpid())
while True:
     pass
signal.alarm(0)
sys.exit(0)
