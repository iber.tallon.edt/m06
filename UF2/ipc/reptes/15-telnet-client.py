#! /usr/bin/python3
#-*- coding: utf-8-*-
#
# Python documentation: https://docs.python.org/es/3/library/subprocess.html
# argparse tutorial: https://docs.python.org/es/3/howto/argparse.html
#  
# iber@edt ASIX M06 Curs 2023-2024
# 
# -----------------------------

#============= Importació de llibreries ==============
import sys, socket, argparse 
from subprocess import Popen, PIPE
parser = argparse.ArgumentParser(description="""ps Client""")
parser.add_argument("server",type=str) # Ha de ser obligatori 
parser.add_argument("-p","--port",type=int, default=50001)
args=parser.parse_args()
HOST = args.server
PORT = args.port

#============= Programa ==============
# Assignar un handler(encarregat de gestionar) al senyal. 

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST, PORT))
while True:
  command = input(" Introdueix una ordre: ")
  s.send(command)
  while True:
    conn = s.accept
    #telnet = 
