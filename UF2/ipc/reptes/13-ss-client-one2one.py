#! /usr/bin/python3
#-*- coding: utf-8-*-
#
# Python documentation: https://docs.python.org/es/3/library/subprocess.html
# argparse tutorial: https://docs.python.org/es/3/howto/argparse.html
#  
# iber@edt ASIX M06 Curs 2023-2024
# 
# -----------------------------

#============= Importació de llibreries ==============
import sys,socket,argparse
parser = argparse.ArgumentParser(description="""Client """)

#============= Definició d'arguments (description, prog, epilog) ==============
parser.add_argument("-s","--server",type=str, default='')
parser.add_argument("-p","--port",type=int, default=50001)
args=parser.parse_args()
HOST = args.server
PORT = args.port

#============= Programa ==============
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((HOST, PORT))
while True:
  ss = s.recv(1024)
  if not ss: break
  print(f'ss -ltp:\n', str(ss))
s.close()
sys.exit(0)