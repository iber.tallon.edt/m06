#! /usr/bin/python3
#-*- coding: utf-8-*-
#
# Python documentation: https://docs.python.org/es/3/library/subprocess.html
# argparse tutorial: https://docs.python.org/es/3/howto/argparse.html
#  
# iber@edt ASIX M06 Curs 2023-2024
# 
# programa.py ruta
# -----------------------------

#============= Importació de llibreries ==============

import sys, argparse
from subprocess import Popen, PIPE

#============= Definició d'arguments (description, prog, epilog) ==============

parser = argparse.ArgumentParser(   
   description="programa d'exemple de Popen",\
   prog="06b-popen.py",\
   epilog="hasta luego lucas!")

# Definició d'un argument a processar.
parser.add_argument("ruta", type=str,\
   help="directori a llistar") 

args = parser.parse_args()


#============= Programa ==============

# Ordre que processa el sistema.
command = [ "ls", args.ruta ]

# Defineix una variable que processa la comanda ("who").
# Qualsevol cosa en majuscula és un contructor.
pipeData = Popen(command, stdout=PIPE)

# Agafa totes les línies que li arriben del túnel(pipe).
for line in pipeData.stdout:
    print(line.decode("utf-8"),end="")
exit(0)
