#! /usr/bin/python3
#-*- coding: utf-8-*-
# iber@edt ASIX M06 Curs 2023-2024
#
# Documentation:
# Python documentation: https://docs.python.org/3/library/argparse.html
# argparse tutorial: https://docs.python.org/es/3/howto/argparse.html
#
# head [-n 5|10|15] file
# default=10, file o stdin
# -----------------------------
import sys, argparse

#============= Definició d'arguments ==============

# Especifico que fa el programa, el nom del fitxer on es troba el programa i un comentari final.
parser = argparse.ArgumentParser(   
   description="programa d'exemple d'arguments",\
   prog="head.py",\
   epilog="Final del anàlisi!")

# choices -> Especifica que l'argument ha de ser o 5 o 10 o 15.
parser.add_argument("-n","--nlin", type=int,\
   dest="nlines", help="número de linies a processar (10)",\
   metavar="num_linies", \
   choices=[5, 10, 15], default=10)

# Paràmetre pasicional -> fit (No necessita valors opcionals)
# Paràmetre opcional -> -f, --file
# Aquests es diferencien per el nom que s'utilitza.
parser.add_argument("fit", type=str, \
  dest="file", help="fitxer a processar (stdin)", metavar="nom_fitxer")

args = parser.parse_args()
print(args)


#============= Programa ==============

num=args.nlines
count=0

fileIn=open(args.file, "r")

for line in fileIn:
    count+=1
    print(line,end="")
    if count==num:
        break
fileIn.close()

exit(0)


#============= Outputs ==============

# Sense cap argument 


# Amb el primer argument 


# Amb els dos arguments
   # python3 reptes/head_choice.py -n 5 -f exemples/ex.1/01-head.py 
      # Namespace(nlines=5, file='exemples/ex.1/01-head.py')
      #! /usr/bin/python3
      #-*- coding: utf-8-*-
      #
      # iber@edt ASIX M06 Curs 2023-2024
      # -----------------------------

# Amb el primer argument incorrecte


# Amb els dos arguments pero amb el primer incorrecte

