#! /usr/bin/python3
#-*- coding: utf-8-*-
#
# Python documentation: https://docs.python.org/es/3/library/subprocess.html
# argparse tutorial: https://docs.python.org/es/3/howto/argparse.html
#  
# iber@edt ASIX M06 Curs 2023-2024
# -----------------------------

#============= Importació de llibreries ==============

import sys, argparse
from subprocess import Popen, PIPE

#============= Programa ==============

# Ordre que processa el sistema.
command = [ "psql -qtA -F',' -h localhost -U postgres training -c \"SELECT * FROM oficinas;\"" ]

# Defineix una variable que processa la comanda ("who").
# Qualsevol cosa en majuscula és un contructor.
pipeData = Popen(command, shell=True, stdout=PIPE)

# Agafa totes les línies que li arriben del túnel(pipe).
for line in pipeData.stdout:
    print(line.decode("utf-8"),end="")
exit(0)
