#! /usr/bin/python3
#-*- coding: utf-8-*-
#
# Python documentation: https://docs.python.org/es/3/library/subprocess.html
# argparse tutorial: https://docs.python.org/es/3/howto/argparse.html
#  
# iber@edt ASIX M06 Curs 2023-2024
# 
# -----------------------------

#============= Importació de llibreries ==============
import sys,socket,argparse
parser = argparse.ArgumentParser(description="""Client """)
parser.add_argument("-s","--server",type=str, default='')
parser.add_argument("-p","--port",type=int, default=50001)
args=parser.parse_args()
host = args.server
port = args.port
s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.connect((host, port))
while True:
  data = s.recv(1024)
  if not data: break
  print('Data:', str(data))
s.close()
sys.exit(0)

