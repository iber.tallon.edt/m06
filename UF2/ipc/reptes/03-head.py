#! /usr/bin/python3
#-*- coding: utf-8-*-
# iber@edt ASIX M06 Curs 2023-2024
#
# Documentation:
# Python documentation: https://docs.python.org/3/library/argparse.html
# argparse tutorial: https://docs.python.org/es/3/howto/argparse.html
#
# head [-n nline] [-f file]
# default=10, file o stdin
# -----------------------------
import sys, argparse

#============= Definició d'arguments ==============

# Especifico que fa el programa, el nom del fitxer on es troba el programa i un comentari final.
parser = argparse.ArgumentParser(   
   description="programa d'exemple d'arguments",\
   prog="head.py",\
   epilog="Final del anàlisi!")

# -n, --nline -> Creo un opció la qual està definida com a -n o com a --nlin,
# type -> especifica el tipus (string, boolea, enter, decimal, ...) que tindrà l'argument,   
# dest -> estableix una variable que utilitzaré en el programa per a identificar l'estat del primer argument a processar, 
# help -> crea un missatge d'ajuda per entendre l'objectiu de l'opció,
# metavar -> diu de forma molt resumida que conté l'argument i
# default -> estableix el valor per defecte que té l'argument.     
parser.add_argument("-n","--nlin", type=int,\
   dest="nlines", help="número de linies a processar (10)",\
   metavar="num_linies", default=10)

parser.add_argument("-f", "--file", type=str, \
  dest="file", help="fitxer a processar (stdin)", metavar="nom_fitxer", \
  default="/dev/stdin")

args = parser.parse_args()
print(args)


#============= Programa ==============

num=args.nlines
count=0

fileIn=open(args.file, "r")

for line in fileIn:
    count+=1
    print(line,end="")
    if count==num:
        break
fileIn.close()

exit(0)



