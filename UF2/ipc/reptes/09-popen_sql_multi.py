#! /usr/bin/python3
#-*- coding: utf-8-*-
#
# Python documentation: https://docs.python.org/es/3/library/subprocess.html
# argparse tutorial: https://docs.python.org/es/3/howto/argparse.html
#  
# iber@edt ASIX M06 Curs 2023-2024
# 
# programa.py -c num_cli -c num_cli ...
# -----------------------------
#============= Importació de llibreries ==============

import sys, argparse
from subprocess import Popen, PIPE


#============= Definició d'arguments (description, prog, epilog) ==============

# action="append" -> Especifica que per cada argument que hi hagi s'ha de afegir 
# a la variable "parser".
parser = argparse.ArgumentParser(description=\
        """Consulta a la base de dades entrada per argument al programa.""")
parser.add_argument('-c',"--client", help='client',type=str,\
        action="append",dest="clieList",required="True")

args=parser.parse_args()


# Aquestes dues línies de codi s'acostumen a posar-se abans de comenzar el programa, 
# ja que s'utilitza per fer validesa de si estan ben definits els arguments 
#for num_clie in args.nclie:
#    print(clie)
#exit(0)

#============= Programa ==============

command = "PGPASSWORD=passwd  psql -qtA -F',' -h localhost  -U postgres training"

# Popen = contructor del túnel que fa connexió entre python i l'ordre que es trobi dins de la variable.
pipeData = Popen(command, shell=True, bufsize=0, \
             universal_newlines=True,
        	 stdin=PIPE, stdout=PIPE, stderr=PIPE)

for num_clie in args.clieList:
    sqlStatement="select * from clientes where num_clie=%s;" % ( num_clie )
    pipeData.stdin.write(sqlStatement+"\n")
    print(pipeData.stdout.readline(), end="")
pipeData.stdin.write("\q\n")
exit(0)


#2111,JCP Inc.,103,50000.00
#2102,First Corp.,101,65000.00
#2103,Acme Mfg.,105,50000.00
