#! /usr/bin/python3
#-*- coding: utf-8-*-
# iber@edt ASIX M06 Curs 2023-2024
#
# Documentation:
# Python documentation: https://docs.python.org/3/library/argparse.html
# argparse tutorial: https://docs.python.org/es/3/howto/argparse.html
#
# head [-n 5|10|15] [-f file]...
# default=10, file o stdin
# -----------------------------
import sys, argparse

#============= Definició d'arguments ==============

# Especifico que fa el programa, el nom del fitxer on es troba el programa i un comentari final.
parser = argparse.ArgumentParser(   
   description="programa d'exemple d'arguments",\
   prog="head.py",\
   epilog="Final del anàlisi!")

# choices -> Especifica que l'argument ha de ser o 5 o 10 o 15.
parser.add_argument("-n","--nlin", type=int,\
   dest="nlines", help="número de linies a processar (10)",\
   metavar="num_linies", \
   choices=[5, 10, 15], default=10)

parser.add_argument("-f", "--file", type=str, \
  dest="fileList", help="fitxer a processar (stdin)", metavar="nom_fitxer", \
  action="append")

args = parser.parse_args()
print(args)

#============= Programa ==============

num=args.nlines

def headfile(file):
   count=0

   fileIn=open(file, "r")

   for line in fileIn:
      count+=1
      print(line,end="")
      if count==num:
         break
   fileIn.close()

for fileName in args.fileList:
   headfile(fileName)

exit(0)


#============= Outputs ==============

# Sense cap argument 
   # ls -l / | python3 head_choice.py 


# Amb el primer argument 

# Amb els dos arguments

# Amb el primer argument incorrecte

# Amb els dos arguments pero amb el primer incorrecte
