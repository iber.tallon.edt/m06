#! /usr/bin/python3
#-*- coding: utf-8-*-
# iber@edt ASIX M06 Curs 2023-2024
#
# Documentation:
# Python documentation: https://docs.python.org/3/library/argparse.html
# argparse tutorial: https://docs.python.org/es/3/howto/argparse.html
#
# head [-n 5|10|15] -v [file ...]
# default=10
# -----------------------------
import sys, argparse

#============= Definició d'arguments ==============

# Especifico que fa el programa, el nom del fitxer on es troba el programa i un comentari final.
parser = argparse.ArgumentParser(   
   description="programa d'exemple d'arguments",\
   prog="head.py",\
   epilog="Final del anàlisi!")

# choices -> Especifica que l'argument ha de ser o 5 o 10 o 15.
parser.add_argument("-n","--nlin", type=int,\
   dest="nlines", help="número de linies a processar (10)",\
   metavar="num_linies", \
   choices=[5, 10, 15], default=10)

parser.add_argument("fileList", type=str, \
  help="fitxer a processar (stdin)", metavar="nom_fitxer", \
  nargs="*")

parser.add_argument('-v', '--verbose', \
   help="mostra el registre del que està passant", \
   action='store_true')

args = parser.parse_args()
print(args)

#============= Programa ==============

num=args.nlines

def headfile(file):
   count=0

   fileIn=open(file, "r")

   for line in fileIn:
      count+=1
      print(line,end="")
      if count==num:
         break
   fileIn.close()

if args.fileList:
   for fileName in args.fileList:
      if args.verbose:
         print("\n", fileName, 40*"-")
      headfile(fileName)

exit(0)


#============= Outputs ==============

# Sense cap argument 
   # python3 reptes/05b-head_multi.py
      # Namespace(nlines=10, fileList=[])

# Amb el primer argument 

# Amb els dos arguments

# Amb el primer argument incorrecte

# Amb els dos arguments pero amb el primer incorrecte
