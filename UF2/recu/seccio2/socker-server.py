#! /usr/bin/python3
#-*- coding: utf-8-*-
#
# iber@edt ASIX M06 Curs 2023-2024
#
# Exàmen de recuperació de la UF2 de M06
#
# 4. Desenvolupament d'un Client/Servidor amb Sockets
# -----------------------------
import sys, socket, argparse, signal
from subprocess import Popen, PIPE
parser = argparse.ArgumentParser(description="server exam")
parser.add_argument("-p", "--port", type=int, metavar="port", dest="port", default="44444")
args = parser.parse_args()
HOST = ''
PORT = args.port

#======== Program ========

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.bind((HOST, PORT))
s.listen(1)
while True:
    conn, addr = s.accept()
    print("Connexió per: ", addr)
    ordre = ['date']
    if not ordre: break
    pipeData = Popen(ordre, stdout=PIPE, stderr=PIPE, shell=True)
    for line in pipeData.stdout:
      conn.send(line)
    conn.close()
s.close
sys.exit(0)
