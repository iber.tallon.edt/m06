#! /usr/bin/python3
#-*- coding: utf-8-*-
#
# iber@edt ASIX M06 Curs 2023-2024
#
# Exàmen de recuperació de la UF2 de M06
#
# 11. Sòcols (Sockets)
# -----------------------------
import sys, socket, argparse, os
from subprocess import Popen, PIPE
parser = argparse.ArgumentParser(description="client exam")
parser.add_argument("-s", "--server", type=str, metavar="server", dest="server", help="Servidor al qual connectar-se")
parser.add_argument("-p", "--port", type=int, metavar="port", dest="port", default="44444")
args = parser.parse_args()
HOST = args.server
PORT = args.port

#======== Program ========

s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
s.connect((HOST, PORT))

while True:
    ordre = s.recv(1024)
    ordre = ordre.stdin.write()
    
s.close()
sys.exit(0)