#! /usr/bin/python3
#-*- coding: utf-8-*-
#
# iber@edt ASIX M06 Curs 2023-2024
#
# Exàmen de recuperació de la UF2 de M06
#
# 12. Execució de Processos amb ‘fork()’ i ‘exec()’
# -----------------------------
import sys,os
print("Hola, començament del programa principal")

pid=os.fork()
if pid != 0:
    print("Procés fill")
    os.execl("/usr/bin/ls","/usr/bin/ls", '.')
sys.exit(0)



