<ins>**Pràctica d’autenticació - pam 2024**</ins>

## Objectius

<p>Configura el container PAM (--privileged) per:</p>

- Permetre l’autenticació local dels usuaris unix locals (unix01, unix02 i unix03)

- Permetre l’autenticació d’usuaris de LDAP. Per fer-ho s’utilitzarà la imatge ldap23:latest (dap23:group) que conté el llistat final d’usuaris i grups LDAP amb els que treballar.

- Als usuaris LDAP se’ls ha de crear el directori home  automàticament si no existeix.

- Als usuaris LDAP se’ls ha de crear un recurs temporal, dins del home anomenat tmp. Un ramdisk de tipus tmpfs de 100M.

- Tots els usuaris han de poder modificar-se el password. Els usuaris locals i també els usuaris de LDAP.<br />
Potser cal refer la configuració ACL de la base de dades LDAP edt.org per permetre que els usuaris es puguin modificar el seu password.
<br />

## Fitxers
<br />

- Dockerfile

    ```
    #pam 2024

    FROM debian:latest
    LABEL author="Iber@edt ASIX-M06"
    LABEL subject="PAM host 2024"
    # ARG DEBIAN_FRONTEND=noninteractive
    RUN apt-get update && apt-get install --fix-missing -y iproute2 nmap vim tree procps less passwd finger libpam-pwquality libpam-mount libnss-ldapd libpam-ldapd nslcd nslcd-utils ldap-utils
    RUN mkdir /opt/docker
    COPY * /opt/docker/
    RUN chmod +x /opt/docker/startup.sh
    WORKDIR /opt/docker
    CMD /opt/docker/startup.sh
    ```
<br />

- startup.sh

    ```
    #! /bin/bash

    for user in unix01 unix02 unix03
    do
    useradd -m -s /bin/bash $user
    echo -e "$user\n$user" | passwd $user
    done

    cp /opt/docker/nslcd.conf /etc/nslcd.conf
    cp /opt/docker/nsswitch.conf /etc/nsswitch.conf
    cp /opt/docker/common-session /etc/pam.d/common-session
    cp /opt/docker/pam_mount.conf.xml /etc/security/pam_mount.conf.xml
    cp /opt/docker/ldap.conf /etc/ldap/ldap.conf

    /usr/sbin/nscd
    /usr/sbin/nslcd
    sleep infinity
    ```
<br />

- ldap.conf

    ```
    #
    # LDAP Defaults
    #

    # See ldap.conf(5) for details
    # This file should be world readable but not world writable.

    BASE	dc=edt,dc=org
    URI	ldap://ldap.edt.org 

    #SIZELIMIT	12
    #TIMELIMIT	15
    #DEREF		never

    # TLS certificates (needed for GnuTLS)
    TLS_CACERT	/etc/ssl/certs/ca-certificates.crt
    ```
<br />

- nslcd.conf

    ```
    # /etc/nslcd.conf
    # nslcd configuration file. See nslcd.conf(5)
    # for details.

    # The user and group nslcd should run as.
    uid nslcd
    gid nslcd

    # The location at which the LDAP server(s) should be reachable.
    uri ldap://ldap.edt.org 

    # The search base that will be used for all queries.
    base dc=edt,dc=org

    # The LDAP protocol version to use.
    #ldap_version 3

    # The DN to bind with for normal lookups.
    #binddn cn=annonymous,dc=example,dc=net
    #bindpw secret

    # The DN used for password modifications by root.
    #rootpwmoddn cn=admin,dc=example,dc=com

    # SSL options
    #ssl off
    #tls_reqcert never
    tls_cacertfile /etc/ssl/certs/ca-certificates.crt

    # The search scope.
    #scope sub
    ``` 
<br />

- nsswitch.conf

    ```
    # /etc/nsswitch.conf
    #
    # Example configuration of GNU Name Service Switch functionality.
    # If you have the `glibc-doc-reference' and `info' packages installed, try:
    # `info libc "Name Service Switch"' for information about this file.

    passwd:         files ldap
    group:          files ldap
    shadow:         files
    gshadow:        files

    hosts:          files dns
    networks:       files

    protocols:      db files
    services:       db files
    ethers:         db files
    rpc:            db files

    netgroup:       nis
    ```
<br />

- pam_mount.conf.xml

    ```
    <?xml version="1.0" encoding="utf-8" ?>
    <!DOCTYPE pam_mount SYSTEM "pam_mount.conf.xml.dtd">
    <!--
        See pam_mount.conf(5) for a description.
    -->

    <pam_mount>

            <!-- debug should come before everything else,
            since this file is still processed in a single pass
            from top-to-bottom -->

    <debug enable="0" />

            <!-- Volume definitions -->

    <volume
        user="*"
        fstype="tmpfs"
        mountpoint="~/tmp"
        options="size=100M"
    />

            <!-- pam_mount parameters: General tunables -->

    <!--
    <luserconf name=".pam_mount.conf.xml" />
    -->

    <!-- Note that commenting out mntoptions will give you the defaults.
        You will need to explicitly initialize it with the empty string
        to reset the defaults to nothing. -->
    <mntoptions allow="nosuid,nodev,loop,encryption,fsck,nonempty,allow_root,allow_other" />
    <!--
    <mntoptions deny="suid,dev" />
    <mntoptions allow="*" />
    <mntoptions deny="*" />
    -->
    <mntoptions require="nosuid,nodev" />

    <!-- requires ofl from hxtools to be present -->
    <logout wait="0" hup="no" term="no" kill="no" />


            <!-- pam_mount parameters: Volume-related -->

    <mkmountpoint enable="1" remove="true" />


    </pam_mount>
    ```
<br />

- common-session

    ```
    #
    # /etc/pam.d/common-session - session-related modules common to all services
    #
    # This file is included from other service-specific PAM config files,
    # and should contain a list of modules that define tasks to be performed
    # at the start and end of interactive sessions.
    #
    # As of pam 1.0.1-6, this file is managed by pam-auth-update by default.
    # To take advantage of this, it is recommended that you configure any
    # local modules either before or after the default block, and use
    # pam-auth-update to manage selection of other modules.  See
    # pam-auth-update(8) for details.

    # here are the per-package modules (the "Primary" block)
    session	[default=1]			pam_permit.so
    # here's the fallback if no module succeeds
    session	requisite			pam_deny.so
    # prime the stack with a positive return value if there isn't one already;
    # this avoids us returning an error just because nothing sets a success code
    # since the modules above will each just jump around
    session	required			pam_permit.so
    # and here are more per-package modules (the "Additional" block)
    session	required	pam_unix.so 
    session optional	pam_mkhomedir.so
    session	optional	pam_mount.so
    session	[success=ok default=ignore]	pam_ldap.so minimum_uid=1000 
    # end of pam-auth-update config
    ```
<br />

- compose.yml

    ```
    version: "3"
    services:
    ldap:
        image: iberedt/ldap23:latest
        container_name: ldap.edt.org
        hostname: ldap.edt.org
        ports:
        - "389:389"
        networks:
        - 2hisx

    pam:
        image: iberedt/pam23:ldap
        container_name: pam.edt.org
        hostname: pam.edt.org
        networks:
        - 2hisx
        privileged: true

    networks:
    2hisx:
    ```
<br />

## Ordres per enxegar els containers

- Enxegar el fitxer compose.yml per poder arrancar els containers pam23:ldap i ldap23:latest = "ldap23:group".

```
$ docker compose up -d
```

- Mirar els processos de docker per comprovar que s'han enxegat de forma correcte.

```
$ docker ps

Sortida:
CONTAINER ID        IMAGE                   COMMAND                  CREATED          STATUS          PORTS                                                  NAMES
416adb0c8e15   iberedt/pam23:ldap      "/bin/sh -c /opt/doc…"   13 seconds ago   Up 12 seconds                                                          pam.edt.org
3288caccf1ff   iberedt/ldap23:latest   "/bin/sh -c /opt/doc…"   13 seconds ago   Up 11 seconds   0.0.0.0:389->389/tcp, :::389->389/tcp                  ldap.edt.org
```

- Entrar interactivament al container de pam23:ldap per poder fer comprovacions de connexió entre containers.

```
$ docker exec -it pam.edt.org /bin/bash
```
<br />

## comprovacions

- Comprovo si el dimoni de "nslcd" que habiem configurat en el **startup.sh** s'ha posat en marxa. 

```
$ ps ax

Sortida:

    PID TTY      STAT   TIME COMMAND
      1 ?        Ss     0:00 /bin/sh -c /opt/docker/startup.sh
      7 ?        S      0:00 /bin/bash /opt/docker/startup.sh
     44 ?        Sl     0:00 /usr/sbin/nslcd
     50 ?        S      0:00 sleep infinity
     51 pts/0    Ss     0:00 /bin/bash
     57 pts/0    R+     0:00 ps ax

```

- Busco si em puc connectar a la base de dades ldpa del container ldap23:latest. En aquest cas ho faré sense necessitat d'autenticar-me.

```
# ldapsearch -x

Sortida: 

# extended LDIF
#
# LDAPv3
# base <dc=edt,dc=org> (default) with scope subtree
# filter: (objectclass=*)
# requesting: ALL
#

# edt.org
dn: dc=edt,dc=org
dc: edt
description: Escola del treball de Barcelona
objectClass: dcObject
objectClass: organization
o: edt.org

# maquines, edt.org
dn: ou=maquines,dc=edt,dc=org
ou: maquines
description: Container per a maquines linux
objectClass: organizationalunit

# clients, edt.org
dn: ou=clients,dc=edt,dc=org
ou: clients
description: Container per a clients linux
objectClass: organizationalunit

# productes, edt.org
dn: ou=productes,dc=edt,dc=org
ou: productes
description: Container per a productes linux
objectClass: organizationalunit

# usuaris, edt.org
dn: ou=usuaris,dc=edt,dc=org
ou: usuaris
description: Container per usuaris del sistema linux
objectClass: organizationalunit

# grups, edt.org
dn: ou=grups,dc=edt,dc=org
ou: grups
description: Container per a grups
objectClass: organizationalunit

# pau, usuaris, edt.org
dn: uid=pau,ou=usuaris,dc=edt,dc=org
objectClass: posixAccount
objectClass: inetOrgPerson
cn: Pau Pou
cn: Pauet Pou
sn: Pou
homePhone: 555-222-2220
mail: pau@edt.org
description: Watch out for this guy
ou: alumnes
uid: pau
uidNumber: 5000
gidNumber: 600
homeDirectory: /tmp/home/pau
userPassword:: e1NTSEF9TkRraXBlc05RcVRGRGdHSmZ5cmFMei9jc1pBSWxrMi8=

# pere, usuaris, edt.org
dn: uid=pere,ou=usuaris,dc=edt,dc=org
objectClass: posixAccount
objectClass: inetOrgPerson
cn: Pere Pou
sn: Pou
homePhone: 555-222-2221
mail: pere@edt.org
description: Watch out for this guy
ou: alumnes
uid: pere
uidNumber: 5001
gidNumber: 600
homeDirectory: /tmp/home/pere
userPassword:: e1NTSEF9Z2htdFJMMTFZdFhvVWhJUDd6NmY3bmI4UkNOYWRGZSs=

# anna, usuaris, edt.org
dn: uid=anna,ou=usuaris,dc=edt,dc=org
objectClass: posixAccount
objectClass: inetOrgPerson
cn: Anna Pou
cn: Anita Pou
sn: Pou
homePhone: 555-222-2222
mail: anna@edt.org
description: Watch out for this girl
ou: alumnes
uid: anna
uidNumber: 5002
gidNumber: 600
homeDirectory: /tmp/home/anna
userPassword:: e1NTSEF9Qm00QjNCdS9mdUg2QmJ5OWxneGZGQXdMWXJLMFJiT3E=

# marta, usuaris, edt.org
dn: uid=marta,ou=usuaris,dc=edt,dc=org
objectClass: posixAccount
objectClass: inetOrgPerson
cn: Marta Mas
sn: Mas
homePhone: 555-222-2223
mail: marta@edt.org
description: Watch out for this girl
ou: professors
uid: marta
uidNumber: 5003
gidNumber: 601
homeDirectory: /tmp/home/marta
userPassword:: e1NTSEF9OSsxRjJmNXZjVzh6L3RtU3pZTldkbHo1R2JEQ3lvT3c=

# jordi, usuaris, edt.org
dn: uid=jordi,ou=usuaris,dc=edt,dc=org
objectClass: posixAccount
objectClass: inetOrgPerson
cn: Jordi Mas
cn: Giorgios Mas
sn: Mas
homePhone: 555-222-2224
mail: jordi@edt.org
description: Watch out for this girl
ou: alumnes
ou: professors
uid: jordi
uidNumber: 5004
gidNumber: 601
homeDirectory: /tmp/home/jordi
userPassword:: e1NTSEF9VDVqck1ncEp3WlpndTBhemtMSVZvWWhpRzA4L0tHVXY=

# admin, usuaris, edt.org
dn: uid=admin,ou=usuaris,dc=edt,dc=org
objectClass: posixAccount
objectClass: inetOrgPerson
cn: Administrador Sistema
cn: System Admin
sn: System
homePhone: 555-222-2225
mail: anna@edt.org
description: Watch out for this girl
ou: system
ou: admin
uid: admin
uidNumber: 10
gidNumber: 27
homeDirectory: /tmp/home/admin
userPassword:: e1NTSEF9NG1TMEZ5Y1djNWJrcFc4L2EzOTZTR05EVFFVbEZTWDM=

# user01, usuaris, edt.org
dn: uid=user01,ou=usuaris,dc=edt,dc=org
objectClass: posixAccount
objectClass: inetOrgPerson
cn: Carlos Matuidi
cn:: dXNlciBDYXJsb3MgTWF0dWlkaSBjdXJzYW50IDJhc2l4IA==
sn: alum1
homePhone: 555-222-0015
mail: user01@edt.org
description: alumne de 2asix
ou: 1asix
uid: user01
uidNumber: 6001
gidNumber: 610
homeDirectory: /tmp/home/2asix/user01
userPassword:: e1NIQX1ZVFJsTlRnNU4yWmlNall4WlRGaE4yRTRNR0ZtT1RobVptUTNPR1JoWlR
 JNU1HVTVZVFEzWmlBZ0xRbz0g

# user02, usuaris, edt.org
dn: uid=user02,ou=usuaris,dc=edt,dc=org
objectClass: posixAccount
objectClass: inetOrgPerson
cn: Manel Torras
cn:: dXNlciBNYW5lbCBUb3JyYXMgY3Vyc2FudCAyYXNpeCA=
sn: alum2
homePhone: 555-222-0016
mail: user02@edt.org
description: alumne de 2asix
ou: 1asix
uid: user02
uidNumber: 6002
gidNumber:: NjEwIA==
homeDirectory: /tmp/home/2asix/user02
userPassword:: e1NIQX1ZbVl3WVRabE5EVmlOVE00TjJKak16SXdNakU0TnpRNVlXSXpZV014WWp
 JNVltUmtOVFEwTmlBZ0xRbz0g

# user03, usuaris, edt.org
dn: uid=user03,ou=usuaris,dc=edt,dc=org
objectClass: posixAccount
objectClass: inetOrgPerson
cn: Xifre el pelos
cn:: dXNlciBYaWZyZSBlbCBwZWxvcyBjdXJzYW50IDJhc2l4IA==
sn: alum3
homePhone: 555-222-0017
mail: user03@edt.org
description: alumne de 2asix
ou: 1asix
uid: user03
uidNumber: 6003
gidNumber:: NjEwIA==
homeDirectory: /tmp/home/2asix/user03
userPassword:: e1NIQX1aalU1TjJZNFkyWTJPRFk1TnpFeFptVmxNelUxTWpFMU5UY3pOR0kzTkR
 Ga1lXTTNaamN4TlNBZ0xRbz0g

# user04, usuaris, edt.org
dn: uid=user04,ou=usuaris,dc=edt,dc=org
objectClass: posixAccount
objectClass: inetOrgPerson
cn: Jaime
cn:: dXNlciBKYWltZSBjdXJzYW50IDJhc2l4IA==
sn: alum4
homePhone: 555-222-0018
mail: user04@edt.org
description: alumne de 2asix
ou: 1asix
uid: user04
uidNumber: 6004
gidNumber:: NjEwIA==
homeDirectory: /tmp/home/2asix/user04
userPassword:: e1NIQX1ZV1kyTWpnNVlUaGpNekU1WXpGaU5qQXpNR05qTjJWbVltVXhObVEwWXp
 Wa01qbGpPR00xWWlBZ0xRbz0g

# user05, usuaris, edt.org
dn: uid=user05,ou=usuaris,dc=edt,dc=org
objectClass: posixAccount
objectClass: inetOrgPerson
cn: Mireia Cano
cn:: dXNlciBNaXJlaWEgQ2FubyBjdXJzYW50IDJhc2l4IA==
sn: alum5
homePhone: 555-222-0019
mail: user05@edt.org
description: alumne de 2asix
ou: 1asix
uid: user05
uidNumber: 6005
gidNumber:: NjEwIA==
homeDirectory: /tmp/home/2asix/user05
userPassword:: e1NIQX1OV05sTW1Fd1lqQTJOakExWldVME1HVTBNamhoWW1ZeVlUTTBNVE0zTXp
 jMk9UWmpZMlJtWkNBZ0xRbz0g

# user06, usuaris, edt.org
dn: uid=user06,ou=usuaris,dc=edt,dc=org
objectClass: posixAccount
objectClass: inetOrgPerson
cn: Lando Cardisian
cn:: dXNlciBMYW5kbyBDYXJkaXNpYW4gY3Vyc2FudCAyYXNpeCA=
sn: alum6
homePhone: 555-222-0020
mail: user06@edt.org
description: alumne de 2asix
ou: 1asix
uid: user06
uidNumber: 6006
gidNumber:: NjEwIA==
homeDirectory: /tmp/home/2asix/user06
userPassword:: e1NIQX1aR1F3WkdNMlpHVm1NV1JpWkRaallXSTFPVFF4TWpobU1UaGtOalE1Wkd
 KaU1ETTVNekJpWkNBZ0xRbz0g

# user07, usuaris, edt.org
dn: uid=user07,ou=usuaris,dc=edt,dc=org
objectClass: posixAccount
objectClass: inetOrgPerson
cn:: U2FyYSA=
cn:: dXNlciBTYXJhIGN1cnNhbnQgMmFzaXgg
sn: alum7
homePhone: 555-222-0021
mail: user07@edt.org
description: alumne de 2asix
ou: 2asix
uid: user07
uidNumber: 6007
gidNumber:: NjExIA==
homeDirectory: /tmp/home/2asix/user07
userPassword:: e1NIQX1PVFkyTkdZMU9USTNOVGczT1dVd05EWTJZMlJtWVRsaU1EWXpOakF3TXp
 ZMk1EWTJZVGRpT0NBZ0xRbz0g

# user08, usuaris, edt.org
dn: uid=user08,ou=usuaris,dc=edt,dc=org
objectClass: posixAccount
objectClass: inetOrgPerson
cn: Chopin
cn:: dXNlciBDaG9waW4gY3Vyc2FudCAyYXNpeCA=
sn: alum8
homePhone: 555-222-0022
mail: user08@edt.org
description: alumne de 2asix
ou: 2asix
uid: user08
uidNumber: 6008
gidNumber:: NjExIA==
homeDirectory: /tmp/home/2asix/user08
userPassword:: e1NIQX1aV0psWkRjME56azFZV00zT1RFMFpEWTNPVE5rWlRobFkyRm1NMll6WWp
 NNE16ZG1PRE01WlNBZ0xRbz0g

# user09, usuaris, edt.org
dn: uid=user09,ou=usuaris,dc=edt,dc=org
objectClass: posixAccount
objectClass: inetOrgPerson
cn: Passacaglia
cn:: dXNlciBQYXNzYWNhZ2xpYSBjdXJzYW50IDJhc2l4IA==
sn: alum9
homePhone: 555-222-0023
mail: user09@edt.org
description: alumne de 2asix
ou: 2asix
uid: user09
uidNumber: 6009
gidNumber:: NjExIA==
homeDirectory: /tmp/home/2asix/user09
userPassword:: e1NIQX1OR0kwTUdSbU56UTVOVFJpWmpWbE16UmpNRGc0TkdJM1lqQTNZV0kwWlR
 NeU1USXdPRFZtWWlBZ0xRbz0g

# user10, usuaris, edt.org
dn: uid=user10,ou=usuaris,dc=edt,dc=org
objectClass: posixAccount
objectClass: inetOrgPerson
cn: Marlen de Dios
cn:: dXNlciBNYXJsZW4gZGUgRGlvcyBjdXJzYW50IDJhc2l4IA==
sn: alum10
homePhone: 555-222-0024
mail: user10@edt.org
description: alumne de 2asix
ou: 2asix
uid: user10
uidNumber: 6010
gidNumber:: NjExIA==
homeDirectory: /tmp/home/2asix/user10
userPassword:: e1NIQX1ZV1ptTVdZMFpUWm1ZMlJrWmpObE16STJZMkpsWkROaE1qTmtOak5tTXp
 FeVlUa3pNVEkyWlNBZ0xRbz0g

# user11, usuaris, edt.org
dn: uid=user11,ou=usuaris,dc=edt,dc=org
objectClass: posixAccount
objectClass: inetOrgPerson
cn: Abdul Rehmen
cn:: dXNlciBBYmR1bCBSZWhtZW4gY3Vyc2FudCAyYXNpeCA=
sn: alum11
homePhone: 555-222-0025
mail: user11@edt.org
description: alumne de 2asix
ou: 2asix
uid: user11
uidNumber: 6011
gidNumber: 611
homeDirectory: /tmp/home/2asix/user11
userPassword:: e1NIQX1NMlZsTlRCaU1qSXdabVl3TWpBME1UTXdZV013TnpRNU5UVTFOekE0WXp
 jeE5UZzRNVEE0TXlBZ0xRbz0g

# professors, grups, edt.org
dn: cn=professors,ou=grups,dc=edt,dc=org
objectClass: posixGroup
cn: professors
gidNumber: 601
description: Grup de professors de l'escola EdT
memberUid: marta
memberUid: jordi

# alumnes, grups, edt.org
dn: cn=alumnes,ou=grups,dc=edt,dc=org
objectClass: posixGroup
cn: alumnes
gidNumber: 600
description: Grup d'alumnes de l'escola EdT
memberUid: pau
memberUid: pere
memberUid: anna
memberUid: jordi

# 1asix, grups, edt.org
dn: cn=1asix,ou=grups,dc=edt,dc=org
objectClass: posixGroup
cn:: MWFzaXgg
gidNumber: 610
description: Grup de 1asix de l'escola EdT
memberUid: user01
memberUid: user02
memberUid: user03
memberUid: user04
memberUid: user05
memberUid: user06

# 2asix, grups, edt.org
dn: cn=2asix,ou=grups,dc=edt,dc=org
objectClass: posixGroup
cn: 2asix
gidNumber: 611
description: Grup de 2asix de l'escola EdT
memberUid: user07
memberUid: user08
memberUid: user09
memberUid: user10
memberUid: user11

# sudo, grups, edt.org
dn: cn=sudo,ou=grups,dc=edt,dc=org
objectClass: posixGroup
cn: sudo
gidNumber: 27
description: Grup de sudoers de l'escola EdT
memberUid: admin

# 1wiam, grups, edt.org
dn: cn=1wiam,ou=grups,dc=edt,dc=org
objectClass: posixGroup
cn: 1wiam
gidNumber: 612
description: Grup de 1wiam de l'escola EdT

# 2wiam, grups, edt.org
dn: cn=2wiam,ou=grups,dc=edt,dc=org
objectClass: posixGroup
cn: 2wiam
gidNumber: 613
description: Grup de 2wiam de l'escola EdT

# 1hiaw, grups, edt.org
dn: cn=1hiaw,ou=grups,dc=edt,dc=org
objectClass: posixGroup
cn: 1hiaw
gidNumber: 614
description: Grup de 1hiaw de l'escola EdT

# search result
search: 2
result: 0 Success

# numResponses: 32
# numEntries: 31
root@pam:/opt/docker# nmap ldap.edt.org
Starting Nmap 7.93 ( https://nmap.org ) at 2024-01-18 08:11 UTC
Nmap scan report for ldap.edt.org (172.22.0.2)
Host is up (0.0000070s latency).
rDNS record for 172.22.0.2: ldap.edt.org.pam23ldap_2hisx
Not shown: 998 closed tcp ports (reset)
PORT    STATE SERVICE
389/tcp open  ldap
636/tcp open  ldapssl
MAC Address: 02:42:AC:16:00:02 (Unknown)

Nmap done: 1 IP address (1 host up) scanned in 0.20 seconds
```

-  Miro si amb el NSS(Name Service Switch) hem troba els usuaris de la base de dades del LDAP i locals. 

```
# getent passwd pere

Sortida:

pere:*:5001:600:Pere Pou:/tmp/home/pere:


# getent passwd unix02

Sortida:

unix02:x:1001:1001::/home/unix02:/bin/bash
```

- Miro si amb el NSS(Name Service Switch) hem troba els grups de la base de dades del LDAP i locals.

```
# getent group

Sortida:

root:x:0:
daemon:x:1:
bin:x:2:
sys:x:3:
adm:x:4:
tty:x:5:
disk:x:6:
lp:x:7:
mail:x:8:
news:x:9:
uucp:x:10:
man:x:12:
proxy:x:13:
kmem:x:15:
dialout:x:20:
fax:x:21:
voice:x:22:
cdrom:x:24:
floppy:x:25:
tape:x:26:
sudo:x:27:
audio:x:29:
dip:x:30:
www-data:x:33:
backup:x:34:
operator:x:37:
list:x:38:
irc:x:39:
src:x:40:
shadow:x:42:
utmp:x:43:
video:x:44:
sasl:x:45:
plugdev:x:46:
staff:x:50:
games:x:60:
users:x:100:
nogroup:x:65534:
messagebus:x:101:
nslcd:x:102:
unix01:x:1000:
unix02:x:1001:
unix03:x:1002:
professors:*:601:jordi,marta
alumnes:*:600:jordi,anna,pau,pere
2asix:*:611:user10,user11,user07,user08,user09
sudo:*:27:admin
1wiam:*:612:
2wiam:*:613:
1hiaw:*:614:
```
<br />

# 

<ins><p align="center">Amb aixó acaba la pràctica d'autenticació amb PAM.</p></ins>

# 
