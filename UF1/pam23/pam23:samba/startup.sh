#! /bin/bash


# Enxegar serveis SAMBA
/usr/sbin/smbd 
/usr/sbin/nmbd  

# Creació dels usuaris locals
for user in unix01 unix02 unix03 unix04 
do
  useradd -m -s /bin/bash $user
  echo -e "$user\n$user" | passwd $user
done

# Copiar les meves configuracions als fitxers de configuracions del container
cp /opt/docker/nslcd.conf /etc/nslcd.conf
cp /opt/docker/nsswitch.conf /etc/nsswitch.conf
cp /opt/docker/common-session /etc/pam.d/common-session
cp /opt/docker/pam_mount.conf.xml /etc/security/pam_mount.conf.xml
cp /opt/docker/ldap.conf /etc/ldap/ldap.conf

# Enxegar serveis per PAM
/usr/sbin/nscd
/usr/sbin/nslcd


sleep infinity
