#! /bin/bash
# @edt ASIX-M06
# --------------------------

# share public
mkdir -p /var/lib/samba/public
chmod 777 /var/lib/samba/public
cp /opt/docker/*  /var/lib/samba/public/.

# share privat
mkdir -p /var/lib/samba/privat
cp /etc/os-release /var/lib/samba/privat/.

# Copiar la configuració
cp /opt/docker/smb.conf /etc/samba/smb.conf

# Enxegar servei SAMBA
/usr/sbin/smbd
/usr/sbin/nmbd

