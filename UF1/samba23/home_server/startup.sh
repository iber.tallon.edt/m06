#! /bin/bash
# iber@edt ASIX-M06
# -------------------------------------


for user in unix01 unix02 unix03 unix04 unix05
do
  useradd -m -s /bin/bash $user
  echo -e "$user\n$user" | passwd $user
done

/usr/sbin/nslcd
/usr/sbin/nscd

# Share public
mkdir -p /var/lib/samba/public
chmod 777 /var/lib/samba/public
cp /opt/docker/* /var/lib/samba/public/.

# Share privat
mkdir -p /var/lib/samba/privat
cp /etc/os-release /var/lib/samba/privat/.

# copiar la configuració
cp /opt/docker/smb.conf /etc/samba/smb.conf

for user in smbunix01 smbunxi02 smbunxi03 smbunxi04 smbunxi05
do
  useradd -m -s /bin/bash $user
  echo -e "$user\n$user" | smbpasswd -a $user
done

chmod +x /opt/docker/ldap_useradd.sh
bash /opt/docker/ldap_useradd.sh

/usr/sbin/smbd 
/usr/sbin/nmbd -F

sleep inifinity
#/bin/bash

