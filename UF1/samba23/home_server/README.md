# <center>SAMBA SERVER</center> 
### Pràctica SAMBA amb PAM+LDAP i homes SAMBA

...


<br /><br /><br />

#### Seguiment connexió entre màquina local i instància AWS

##### Requisits:
- Crear-se una ip elàstica en AWS i assignar-la a la instància.
- Deixar passar els ports de LDAP (389) i SAMBA (139 i 445) en la instància de LDAP des del security group. 
- Tenir les teves imatges pujades en el teu Docker Hub.
<br />

##### Seguiment

###### AWS

1. Entrar remotament des de la meva màquina local a la meva instància.
    ```
    ssh -i ~/.ssh/samba_server.pem ec2-user@54.224.97.29
    ```

    - Si surt el seguent error: <i>"Load key xxx/.ssh/samba_server.pem": bad permissions"</i>, canvio els permisos de la clau privada:
        ```
        chmod 400 ~/.ssh/samba_server.pem
        ``` 

2. Faig la instalació de Docker.
<br />

3. Creo un fitxer **compose.yml** que crea un container ldap i un container samba propagant els dos els seus ports.
    ```
    version: "3"
    services:
        ldap:
            image: iberedt/ldap23:latest
            privileged: true
            container_name: ldap.edt.org
            hostname: ldap.edt.org
            ports:
                - "389:389"
            networks:
                - 2hisx
        samba:
            image: iberedt/samba23:home_server
            privileged: true
            container_name: samba.edt.org
            hostname: samba.edt.org
            ports:
                - "139:139"
                - "445:445"
            networks:
                - 2hisx
    networks:
        2hisx:
    ```
<br />

4. Encendre els dos containers dins de la instància.
    ```
    docker-compose up -d
    ```

    - Comprovo que s'han creat els containers.
        ```
        docker ps
        CONTAINER ID   IMAGE                         COMMAND                  CREATED         STATUS         PORTS                                                                          NAMES
        135ada00bfec   iberedt/ldap23:latest         "/bin/sh -c /opt/doc…"   4 seconds ago   Up 2 seconds   0.0.0.0:389->389/tcp, :::389->389/tcp                                          ldap.edt.org
        babb538344b9   iberedt/samba23:home_server   "/bin/sh -c '/opt/do…"   4 seconds ago   Up 2 seconds   0.0.0.0:139->139/tcp, :::139->139/tcp, 0.0.0.0:445->445/tcp, :::445->445/tcp   samba.edt.org
        ```
    
    - Comprovo que s'han propagat els ports.
        ```
        nmap localhost
        
        Starting Nmap 7.93 ( https://nmap.org ) at 2024-04-11 11:37 UTC
        Nmap scan report for localhost (127.0.0.1)
        Host is up (0.00028s latency).
        Not shown: 996 closed tcp ports (conn-refused)
        PORT    STATE SERVICE
        22/tcp  open  ssh
        139/tcp open  netbios-ssn       <---
        389/tcp open  ldap              <---
        445/tcp open  microsoft-ds      <---

        Nmap done: 1 IP address (1 host up) scanned in 0.13 seconds
        ```
<br />

###### Màquina local

5. Entro interactivament al container de PAM.
    ```
    docker run --rm -h pam.edt.org --name pam.edt.org --net 2hisx --privileged -it iberedt/pam23:samba /bin/bash
    ```
<br />

6. Afegeixo a la última línia del fitxer <i>/etc/hosts</i> el següent per que la meva màquina pogui connectar-se amb la instància de AWS.
    ```
    ...
    54.224.97.29	ldap.edt.org samba.edt.org
    ```
<br />

7. Enxego el meu script i des d'un altre sessió entro interactivament.
    - Enxegar el script
        ```
        bash startup.sh
        ```
    - Entrar interactivament
        ```
        docker exec -it pam.edt.org /bin/bash
        ```
<br />

##### Comprovacions
###### AWS
```
docker exec -it samba.edt.org /bin/bash

# Mostra l'informació del directori /etc/passwd 
root@samba:/opt/docker# getent passwd
...
unix01:x:1000:1000::/home/unix01:/bin/bash
unix02:x:1001:1001::/home/unix02:/bin/bash
unix03:x:1002:1002::/home/unix03:/bin/bash
unix04:x:1003:1003::/home/unix04:/bin/bash
unix05:x:1004:1004::/home/unix05:/bin/bash
smbunix01:x:1005:1005::/home/smbunix01:/bin/bash
smbunxi02:x:1006:1006::/home/smbunxi02:/bin/bash
smbunxi03:x:1007:1007::/home/smbunxi03:/bin/bash
smbunxi04:x:1008:1008::/home/smbunxi04:/bin/bash
smbunxi05:x:1009:1009::/home/smbunxi05:/bin/bash
pau:*:5000:100:Pau Pou:/tmp/home/pau:
pere:*:5001:100:Pere Pou:/tmp/home/pere:
anna:*:5002:600:Anna Pou:/tmp/home/anna:
marta:*:5003:600:Marta Mas:/tmp/home/marta:
jordi:*:5004:100:Jordi Mas:/tmp/home/jordi:
admin:*:10:10:Administrador Sistema:/tmp/home/admin:
user01:*:7001:610:user01:/tmp/home/1asix/user01:
user02:*:7002:610:user02:/tmp/home/1asix/user02:
user02:*:7003:610:user03:/tmp/home/1asix/user03:
user03:*:7003:610:user03:/tmp/home/1asix/user03:
user04:*:7004:610:user04:/tmp/home/1asix/user04:
user05:*:7005:610:user05:/tmp/home/1asix/user05:
user06:*:7006:611:user06:/tmp/home/2asix/user06:
user07:*:7007:611:user07:/tmp/home/2asix/user07:
user08:*:7008:611:user08:/tmp/home/2asix/user08:
user09:*:7009:611:user09:/tmp/home/2asix/user09:
user10:*:7010:611:user10:/tmp/home/2asix/user10:

# Mostrar la base de dades dels usuaris SAMBA
root@samba:/opt/docker# pdbedit -vL
---------------
Unix username:        user03
NT username:          
Account Flags:        [U          ]
User SID:             S-1-5-21-4019472965-3800232582-2001942199-1013
Primary Group SID:    S-1-5-21-4019472965-3800232582-2001942199-513
Full Name:            user03
Home Directory:       \\SAMBA\user03
HomeDir Drive:        
Logon Script:         
Profile Path:         \\SAMBA\user03\profile
Domain:               SAMBA
...

# Mostra els recursos que té el servidor.
root@samba:/opt/docker# testparm
Load smb config files from /etc/samba/smb.conf
WARNING: [printers] service MUST be printable!
Loaded services file OK.
Weak crypto is allowed by GnuTLS (e.g. NTLM as a compatibility fallback)

Server role: ROLE_STANDALONE

Press enter to see a dump of your service definitions

# Global parameters
[global]
	log file = /var/log/samba/log.%m
	max log size = 50
	security = USER
	server string = Samba Server Version %v
	idmap config * : backend = tdb
	cups options = raw

[homes]
	browseable = No
	comment = Home Directories
	read only = No

[printers]
	browseable = No
	comment = All Printers
	path = /var/sloop/samba
	printable = Yes

[documentation]
	comment = Documentació doc del container
	guest ok = Yes
	path = /usr/share/doc

[manpages]
	comment = Documentació man del container
	guest ok = Yes
	path = /usr/share/man

[public]
	comment = Share de contingut public
	guest ok = Yes
	path = /var/lib/samba/public
	read only = No

[privat]
	browseable = No
	comment = Share d'accés privat
	guest ok = Yes
	path = /var/lib/samba/privat
	read only = No
```
<br />

###### Màquina local
```
# Entrar dins dels recurs de marta com a usuari marta.
root@pam:/opt/docker# smbclient -U marta%marta //samba.edt.org/marta  
Try "help" to get a list of possible commands.
smb: \> ls  
  .                                   D        0  Tue Sep 19 00:00:00 2023
  ..                                  D        0  Fri Apr 12 07:47:30 2024
  .bash_logout                        H      220  Sun Apr 23 21:23:06 2023
  .bashrc                             H     3526  Sun Apr 23 21:23:06 2023
  .profile                            H      807  Sun Apr 23 21:23:06 2023

		8310764 blocks of size 1024. 4997224 blocks available
smb: \> pwd
Current directory is \\samba.edt.org\marta\

# Connectar-se com a usuari pere 
root@pam:/opt/docker# su -l pere
Creating directory '/tmp/home/pere'.
reenter password for pam_mount:
$ bash

# Connectar-se com a usuari marta
pere@pam:~$ su -l marta
Password: 
Creating directory '/tmp/home/marta'.
$ bash 
marta@pam:~$ id
uid=5003(marta) gid=600(alumnes) groups=600(alumnes)
marta@pam:~$ pwd
/tmp/home/marta
marta@pam:~$ ls -l
total 0
drwxr-xr-x 2 marta alumnes  0 Sep 19  2023 marta
drwxrwxrwt 2 root  alumnes 40 Apr 12 07:53 mytmp
```