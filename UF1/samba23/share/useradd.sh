#! /bin/bash
#
# 
# Iber
# AISX-M06
# Creació de usuaris samba

for user in smbunix01 smbunix02 smbunix03 smbunix04 smbunix05
do
  useradd -m -s /bin/bash $user
  echo -e "$user\n$user" | smbpasswd -a $user
done

