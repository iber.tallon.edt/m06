#! /bin/bash

# 1) Borra tota la configuració i les dades predetermindes de ldap.
rm -rf /var/lib/ldap/*
rm -rf /etc/ldap/slapd.d/*

# 2) Primer bolco la meva configuració que jo he creat al directori de configuració dinàmica.
slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d
# 3) Afegeixo les dades del fitxer ldif.
slapadd -F /etc/ldap/slapd.d -l /opt/docker/edt-org.ldif
# 4) Mirar les noves dades.
slapcat

# 5) Assignar la propietat i el grup dels directoris al usuari openldap(client ldap).
chgrp -R openldap.openldap /var/lib/ldap /etc/ldap/slapd.d
# 6) Enxega el dimoni de ldap "slapd".
/usr/sbin/slapd -d0

