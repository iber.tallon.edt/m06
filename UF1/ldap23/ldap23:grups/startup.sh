#! /bin/bash

rm -rf /var/lib/ldap/*
rm -rf /etc/ldap/slapd.d/*

slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d
slapadd -F /etc/ldap/slapd.d -l /opt/docker/edt-org.ldif
slapcat

chown -R openldap:openldap /var/lib/ldap /etc/ldap/slapd.d
cp /opt/docker/ldap.conf /etc/ldap/ldap.conf
/usr/sbin/slapd -d0
