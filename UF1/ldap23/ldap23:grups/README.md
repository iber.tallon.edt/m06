## Pràctica ldap23:grups en el curs 2023/2024

### Pasos fets:
**1. Creacióde fitxers:**
* Dockerfile
```
# servidor ldap 

FROM debian:latest
LABEL author="Iber@edt ASIX-M06"
LABEL subject="ldapserver 2023"
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get update && apt-get install -y iproute2 nmap vim tree procps less ldap-utils slapd
RUN mkdir /opt/docker
COPY * /opt/docker/
RUN chmod +x /opt/docker/startup.sh
WORKDIR /opt/docker
CMD /opt/docker/startup.sh
EXPOSE 389            
```

* startup.sh
```
#! /bin/bash

rm -rf /var/lib/ldap/*
rm -rf /etc/ldap/slapd.d/*

slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d
slapadd -F /etc/ldap/slapd.d -l /opt/docker/edt-org.ldif
slapcat

chown -R openldap:openldap /var/lib/ldap /etc/ldap/slapd.d
cp /opt/docker/ldap.conf /etc/ldap/ldap.conf
/usr/sbin/slapd -d0 -h 'ldap:/// ldaps:/// ldapi///'
```

* slapd.conf
```
#
# See slapd.conf(5) for details on configuration options.
# This file should NOT be world readable.
#
# debian packages: slapd ldap-utils

#include		/etc/ldap/schema/corba.schema
#include         /etc/ldap/schema/duaconf.schema
#include         /etc/ldap/schema/dyngroup.schema
#include         /etc/ldap/schema/java.schema
#include         /etc/ldap/schema/misc.schema
#include         /etc/ldap/schema/ppolicy.schema
#include         /etc/ldap/schema/collective.schema
include		/etc/ldap/schema/core.schema
include		/etc/ldap/schema/cosine.schema
include		/etc/ldap/schema/inetorgperson.schema
include		/etc/ldap/schema/nis.schema
include		/etc/ldap/schema/openldap.schema

# Allow LDAPv2 client connections.  This is NOT the default.
allow bind_v2

pidfile		/var/run/slapd/slapd.pid
#argsfile	/var/run/openldap/slapd.args

modulepath /usr/lib/ldap
moduleload back_mdb.la
moduleload back_monitor.la

# ----------------------------------------------------------------------
database mdb
suffix "dc=edt,dc=org"
rootdn "cn=Manager,dc=edt,dc=org"
rootpw {SSHA}WbS2P+U5Huv24E5/AMZUafFvI6MNM8DW 
# rootpw = secret
directory /var/lib/ldap
index objectClass eq,pres
access to * by self write by * read
# ---------------------------------------------------------------------
database config
rootdn "cn=sysadmin,cn=config"
rootpw syskey
# ----------------------------------------------------------------------
database monitor
access to *
       by dn.exact="cn=Manager,dc=edt,dc=org" read
       by * none
```

* edt-org.ldif
```
dn: dc=edt,dc=org
dc: edt
description: Escola del treball de Barcelona
objectClass: dcObject
objectClass: organization
o: edt.org

dn: ou=maquines,dc=edt,dc=org
ou: maquines
description: Container per a maquines linux
objectclass: organizationalunit

dn: ou=clients,dc=edt,dc=org
ou: clients
description: Container per a clients linux
objectclass: organizationalunit

dn: ou=productes,dc=edt,dc=org
ou: productes
description: Container per a productes linux
objectclass: organizationalunit

dn: ou=usuaris,dc=edt,dc=org
ou: usuaris
description: Container per usuaris del sistema linux
objectclass: organizationalunit

dn: ou=grups,dc=edt,dc=org
ou: grups
description: Container per a grups
objectclass: organizationalunit

# --------------------------------------------------------------------- (usuaris)

dn: uid=pau,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: Pau Pou
cn: Pauet Pou
sn: Pou
homephone: 555-222-2220
mail: pau@edt.org
description: Watch out for this guy
ou: alumnes
uid: pau
uidNumber: 5000
gidNumber: 600
homeDirectory: /tmp/home/pau
userPassword: {SSHA}NDkipesNQqTFDgGJfyraLz/csZAIlk2/

dn: uid=pere,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: Pere Pou
sn: Pou
homephone: 555-222-2221
mail: pere@edt.org
description: Watch out for this guy
ou: alumnes
uid: pere
uidNumber: 5001
gidNumber: 600
homeDirectory: /tmp/home/pere
userPassword: {SSHA}ghmtRL11YtXoUhIP7z6f7nb8RCNadFe+

dn: uid=anna,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: Anna Pou
cn: Anita Pou
sn: Pou
homephone: 555-222-2222
mail: anna@edt.org
description: Watch out for this girl
ou: alumnes
uid: anna
uidNumber: 5002
gidNumber: 600
homeDirectory: /tmp/home/anna
userPassword: {SSHA}Bm4B3Bu/fuH6Bby9lgxfFAwLYrK0RbOq

dn: uid=marta,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: Marta Mas
sn: Mas
homephone: 555-222-2223
mail: marta@edt.org
description: Watch out for this girl
ou: professors
uid: marta
uidNumber: 5003
gidNumber: 601
homeDirectory: /tmp/home/marta
userPassword: {SSHA}9+1F2f5vcW8z/tmSzYNWdlz5GbDCyoOw

dn: uid=jordi,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: Jordi Mas
cn: Giorgios Mas
sn: Mas
homephone: 555-222-2224
mail: jordi@edt.org
description: Watch out for this girl
ou: alumnes
ou: professors
uid: jordi
uidNumber: 5004
gidNumber: 601
homeDirectory: /tmp/home/jordi
userPassword: {SSHA}T5jrMgpJwZZgu0azkLIVoYhiG08/KGUv

dn: uid=admin,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: Administrador Sistema
cn: System Admin
sn: System
homephone: 555-222-2225
mail: anna@edt.org
description: Watch out for this girl
ou: system
ou: admin
uid: admin
uidNumber: 10
gidNumber: 27
homeDirectory: /tmp/home/admin
userPassword: {SSHA}4mS0FycWc5bkpW8/a396SGNDTQUlFSX3

# --------------------------------------------------------------------- (users)

dn: uid=user01,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: Carlos Matuidi
cn: user Carlos Matuidi cursant 2asix 
sn: alum1
homephone: 555-222-0015
mail: user01@edt.org
description: alumne de 2asix
ou: 1asix
uid: user01
uidNumber: 6001
gidNumber: 610
homeDirectory: /tmp/home/2asix/user01
userPassword: {SHA}YTRlNTg5N2ZiMjYxZTFhN2E4MGFmOThmZmQ3OGRhZTI5MGU5YTQ3ZiAgLQo= 
#(passwd = user01)

dn: uid=user02,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: Manel Torras
cn: user Manel Torras cursant 2asix 
sn: alum2
homephone: 555-222-0016
mail: user02@edt.org
description: alumne de 2asix
ou: 1asix
uid: user02
uidNumber: 6002
gidNumber: 610 
homeDirectory: /tmp/home/2asix/user02
userPassword: {SHA}YmYwYTZlNDViNTM4N2JjMzIwMjE4NzQ5YWIzYWMxYjI5YmRkNTQ0NiAgLQo= 
#(passwd = user02)

dn: uid=user03,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: Xifre el pelos
cn: user Xifre el pelos cursant 2asix 
sn: alum3
homephone: 555-222-0017
mail: user03@edt.org
description: alumne de 2asix
ou: 1asix
uid: user03
uidNumber: 6003
gidNumber: 610 
homeDirectory: /tmp/home/2asix/user03
userPassword: {SHA}ZjU5N2Y4Y2Y2ODY5NzExZmVlMzU1MjE1NTczNGI3NDFkYWM3ZjcxNSAgLQo= 
#(passwd = user03)

dn: uid=user04,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: Jaime
cn: user Jaime cursant 2asix 
sn: alum4
homephone: 555-222-0018
mail: user04@edt.org
description: alumne de 2asix
ou: 1asix
uid: user04
uidNumber: 6004
gidNumber: 610 
homeDirectory: /tmp/home/2asix/user04
userPassword: {SHA}YWY2Mjg5YThjMzE5YzFiNjAzMGNjN2VmYmUxNmQ0YzVkMjljOGM1YiAgLQo= 
#(passwd = user04)

dn: uid=user05,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: Mireia Cano
cn: user Mireia Cano cursant 2asix 
sn: alum5
homephone: 555-222-0019
mail: user05@edt.org
description: alumne de 2asix
ou: 1asix
uid: user05
uidNumber: 6005
gidNumber: 610 
homeDirectory: /tmp/home/2asix/user05
userPassword: {SHA}NWNlMmEwYjA2NjA1ZWU0MGU0MjhhYmYyYTM0MTM3Mzc2OTZjY2RmZCAgLQo= 
#(passwd = user05)

dn: uid=user06,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: Lando Cardisian
cn: user Lando Cardisian cursant 2asix 
sn: alum6
homephone: 555-222-0020
mail: user06@edt.org
description: alumne de 2asix
ou: 1asix
uid: user06
uidNumber: 6006
gidNumber: 610 
homeDirectory: /tmp/home/2asix/user06
userPassword: {SHA}ZGQwZGM2ZGVmMWRiZDZjYWI1OTQxMjhmMThkNjQ5ZGJiMDM5MzBiZCAgLQo= 
#(passwd = user06)

dn: uid=user07,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: Sara 
cn: user Sara cursant 2asix 
sn: alum7
homephone: 555-222-0021
mail: user07@edt.org
description: alumne de 2asix
ou: 2asix
uid: user07
uidNumber: 6007
gidNumber: 611 
homeDirectory: /tmp/home/2asix/user07
userPassword: {SHA}OTY2NGY1OTI3NTg3OWUwNDY2Y2RmYTliMDYzNjAwMzY2MDY2YTdiOCAgLQo= 
#(passwd = user07)

dn: uid=user08,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: Chopin
cn: user Chopin cursant 2asix 
sn: alum8
homephone: 555-222-0022
mail: user08@edt.org
description: alumne de 2asix
ou: 2asix
uid: user08
uidNumber: 6008
gidNumber: 611 
homeDirectory: /tmp/home/2asix/user08
userPassword: {SHA}ZWJlZDc0Nzk1YWM3OTE0ZDY3OTNkZThlY2FmM2YzYjM4MzdmODM5ZSAgLQo= 
#(passwd = user08)

dn: uid=user09,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: Passacaglia
cn: user Passacaglia cursant 2asix 
sn: alum9
homephone: 555-222-0023
mail: user09@edt.org
description: alumne de 2asix
ou: 2asix
uid: user09
uidNumber: 6009
gidNumber: 611 
homeDirectory: /tmp/home/2asix/user09
userPassword: {SHA}NGI0MGRmNzQ5NTRiZjVlMzRjMDg4NGI3YjA3YWI0ZTMyMTIwODVmYiAgLQo= 
#(passwd = user09)

dn: uid=user10,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: Marlen de Dios
cn: user Marlen de Dios cursant 2asix 
sn: alum10
homephone: 555-222-0024
mail: user10@edt.org
description: alumne de 2asix
ou: 2asix
uid: user10
uidNumber: 6010
gidNumber: 611 
homeDirectory: /tmp/home/2asix/user10
userPassword: {SHA}YWZmMWY0ZTZmY2RkZjNlMzI2Y2JlZDNhMjNkNjNmMzEyYTkzMTI2ZSAgLQo= 
#(passwd = user10)

dn: uid=user11,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: Abdul Rehmen
cn: user Abdul Rehmen cursant 2asix 
sn: alum11
homephone: 555-222-0025
mail: user11@edt.org
description: alumne de 2asix
ou: 2asix
uid: user11
uidNumber: 6011
gidNumber: 611
homeDirectory: /tmp/home/2asix/user11
userPassword: {SHA}M2VlNTBiMjIwZmYwMjA0MTMwYWMwNzQ5NTU1NzA4YzcxNTg4MTA4MyAgLQo= 
#(passwd = user11)

# ---------------------------------------------------------------------- (grups)

dn: cn=professors,ou=grups,dc=edt,dc=org
objectclass: posixGroup
cn: professors
gidNumber: 601
description: Grup de professors de l'escola EdT
memberUid: marta
memberUid: jordi

dn: cn=alumnes,ou=grups,dc=edt,dc=org
objectclass: posixGroup
cn: alumnes
gidNumber: 600
description: Grup d'alumnes de l'escola EdT
memberUid: pau
memberUid: pere
memberUid: anna
memberUid: jordi

dn: cn=1asix,ou=grups,dc=edt,dc=org
objectclass: posixGroup
cn: 1asix 
gidNumber: 610
description: Grup de 1asix de l'escola EdT
memberUid: user01
memberUid: user02
memberUid: user03
memberUid: user04
memberUid: user05
memberUid: user06

dn: cn=2asix,ou=grups,dc=edt,dc=org
objectclass: posixGroup
cn: 2asix
gidNumber: 611
description: Grup de 2asix de l'escola EdT
memberUid: user07
memberUid: user08
memberUid: user09
memberUid: user10
memberUid: user11

dn: cn=sudo,ou=grups,dc=edt,dc=org
objectclass: posixGroup
cn: sudo
gidNumber: 27
description: Grup de sudoers de l'escola EdT
memberUid: admin

dn: cn=1wiam,ou=grups,dc=edt,dc=org
objectclass: posixGroup
cn: 1wiam
gidNumber: 612
description: Grup de 1wiam de l'escola EdT

dn: cn=2wiam,ou=grups,dc=edt,dc=org
objectclass: posixGroup
cn: 2wiam
gidNumber: 613
description: Grup de 2wiam de l'escola EdT

dn: cn=1hiaw,ou=grups,dc=edt,dc=org
objectclass: posixGroup
cn: 1hiaw
gidNumber: 614
description: Grup de 1hiaw de l'escola EdT

```

* ldap.conf
```
#
# LDAP Defaults
#

# See ldap.conf(5) for details
# This file should be world readable but not world writable.

#BASE	dc=example,dc=com
#URI	ldap://ldap.example.com ldap://ldap-provider.example.com:666

#SIZELIMIT	12
#TIMELIMIT	15
#DEREF		never

# TLS certificates (needed for GnuTLS)
TLS_CACERT	/etc/ssl/certs/ca-certificates.crt

BASE dc=edt,dc=org
URI ldap://ldap.edt.org
```
**2. Creo la imatge "ldap23:grups" i entro dins d'ella:**
```
docker run --rm --name ldap.edt.org -h ldap.edt.org --net 2hisx -p 389:389 -d iberedt/ldap23:grups

docker exec -it ldap.edt.org /bin/bash
```
***
### Comprovacions
* **Buscar totes les dades dins de la base de dades "edt.org":**
```
ldapsearch -x -LLL -b "dc=edt,dc=org"
```
* **Buscar els usuaris dins del grup professors:**
```
ldapsearch -x -LLL -b "cn=professors,ou=grups,dc=edt,dc=org" 'memberUid'

# output
dn: cn=professors,ou=grups,dc=edt,dc=org
memberUid: marta
memberUid: jordi
```

* **Buscar els usuaris dins del grup 1asix:**
```
ldapsearch -x -LLL -b "cn=1asix,ou=grups,dc=edt,dc=org" 'memberUid'

# output
dn: cn=1asix,ou=grups,dc=edt,dc=org
memberUid: user01
memberUid: user02
memberUid: user03
memberUid: user04
memberUid: user05
memberUid: user06
```
* **Buscar els usuaris dins del grup sudo:**
```
ldapsearch -x -LLL -b "cn=sudo,ou=grups,dc=edt,dc=org" 'memberUid'

# output
dn: cn=sudo,ou=grups,dc=edt,dc=org
memberUid: admin
```

