## Pràctica schema LDAP 2023-2024

### M06-ASO Pràctica (2) LDAP PRACTICA

#### Pasos fets:

**1. Creació de fitxes:**

*Dockerfile
```
# ldapserver 2023
FROM debian:latest
LABEL version="1.0"
LABEL author="iber@edt ASIX-M06"
LABEL subject="ldapserver 2023"
RUN apt-get update
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get install -y procps iproute2 tree nmap vim ldap-utils slapd
RUN mkdir /opt/docker
COPY * /opt/docker/
RUN chmod +x /opt/docker/startup.sh
WORKDIR /opt/docker
CMD /opt/docker/startup.sh
EXPOSE 389
```

*startup.sh
```
#! /bin/bash

echo "Inicialització de la BD de ldap edt.org"

rm -rf /etc/ldap/slapd.d/*
rm -rf /var/lib/ldap/*
slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d
slapadd -F /etc/ldap/slapd.d/ -l /opt/docker/edt-org.ldif
slapcat
chown -R openldap:openldap /etc/ldap/slapd.d /var/lib/ldap
cp /opt/docker/ldap.conf /etc/ldap/ldap.conf
/usr/sbin/slapd -d0
```

*slapd.conf
```
#
# See slapd.conf(5) for details on configuration options.
# This file should NOT be world readable.
#
# debian packages: slapd ldap-utils

include		/etc/ldap/schema/corba.schema
include		/etc/ldap/schema/core.schema
include		/etc/ldap/schema/cosine.schema
include		/etc/ldap/schema/duaconf.schema
include		/etc/ldap/schema/dyngroup.schema
include		/etc/ldap/schema/inetorgperson.schema
include		/etc/ldap/schema/java.schema
include		/etc/ldap/schema/misc.schema
include		/etc/ldap/schema/nis.schema
include		/etc/ldap/schema/openldap.schema
# include		/etc/ldap/schema/ppolicy.schema
include		/etc/ldap/schema/collective.schema
include		/opt/docker/practica.schema

# Allow LDAPv2 client connections.  This is NOT the default.
allow bind_v2

pidfile		/var/run/slapd/slapd.pid
#argsfile	/var/run/openldap/slapd.args

modulepath /usr/lib/ldap
moduleload back_mdb.la
moduleload back_monitor.la

# ----------------------------------------------------------------------
database mdb
suffix "dc=edt,dc=org"
rootdn "cn=Manager,dc=edt,dc=org"
rootpw secret
directory /var/lib/ldap
index objectClass eq,pres
access to * by self write by * read
# ----------------------------------------------------------------------
database monitor
access to *
       by dn.exact="cn=Manager,dc=edt,dc=org" read
       by * none
```

*edt-org.ldif
```
# Crear la organización
dn: dc=edt,dc=org
objectClass: top
objectClass: dcObject
objectClass: organization
o: edt.org
dc: edt

dn: ou=practica,dc=edt,dc=org
objectClass: top
objectClass: organizationalunit
ou: practica
description: Contanidor per empreses

dn: x-nom=lisot,ou=practica,dc=edt,dc=org
objectClass: x-desctec
objectClass: x-descadd
x-nom: lisot
x-descripcio: Empresa dedicada al mantenimiento informatic i ciberseguretat
x-funcionament: TRUE
x-document:< file:/opt/docker/lisot.pdf
x-direccio: Carrer de la pusilanime, 27, Castelldefels, 08058
x-photo:< file:/opt/docker/lisot.png

dn: x-nom=pulisher,ou=practica,dc=edt,dc=org
objectClass: x-desctec
objectClass: x-descadd
x-nom: pulisher
x-descripcio: Empresa dedicada al fraude informiatic 
x-funcionament: FALSE
x-direccio: Carrer dels carruatges tunejats, 50, Vall d'Aran, 04358
x-photo:< file:/opt/docker/pulisher.png

dn: x-nom=postres,ou=practica,dc=edt,dc=org
objectClass: x-desctec
objectClass: x-descadd
x-nom: postres
x-descripcio: Empresa dedicada a reposteria
x-funcionament: TRUE
x-document:< file:/opt/docker/postres.pdf
x-direccio: Carrer camila III, 103, Madrid, 32030
x-photo:< file:/opt/docker/postres.jpg
```

*ldap.conf
```
#
# LDAP Defaults
#

# See ldap.conf(5) for details
# This file should be world readable but not world writable.

#BASE	dc=example,dc=com
#URI	ldap://ldap.example.com ldap://ldap-provider.example.com:666

#SIZELIMIT	12
#TIMELIMIT	15
#DEREF		never

# TLS certificates (needed for GnuTLS)
TLS_CACERT	/etc/ssl/certs/ca-certificates.crt

BASE dc=edt,dc=org
URI ldap.edt.org://ldap
```

*practica.schema
```
# practica.schema
#
# x-desctec:
# 	x-nom
# 	x-descripcio
#	x-funcionament
# 	x-document
#
# x-descadd:
# 	x-direccio 
#		(carrer, ciutat i codi postal) 
#	x-photo
# 
# -------------------------------------

attributetype (1.1.2.1.1.1 NAME 'x-nom'
 DESC 'nom de la empresa'
 EQUALITY caseIgnoreMatch
 SUBSTR caseIgnoreSubstringsMatch
 SYNTAX 1.3.6.1.4.1.1466.115.121.1.15
 SINGLE-VALUE)

attributetype (1.1.2.1.1.2 NAME 'x-descripcio'
 DESC 'descripció de la empresa'
 EQUALITY caseIgnoreMatch
 SUBSTR caseIgnoreSubstringsMatch
 SYNTAX 1.3.6.1.4.1.1466.115.121.1.15
 SINGLE-VALUE)

attributetype (1.1.2.1.1.3 NAME 'x-funcionament'
 DESC 'actualment en funcionament: true/false'
 SYNTAX 1.3.6.1.4.1.1466.115.121.1.7
 SINGLE-VALUE)

attributetype (1.1.2.1.1.4 NAME 'x-document'
 DESC 'document pdf sober la empresa'
 SYNTAX 1.3.6.1.4.1.1466.115.121.1.5
 SINGLE-VALUE)

attributetype (1.1.2.1.1.5 NAME 'x-direccio'
 DESC 'direcció on esa situada la empresa'
 EQUALITY caseIgnoreMatch
 SUBSTR caseIgnoreSubstringsMatch
 SYNTAX 1.3.6.1.4.1.1466.115.121.1.15
 SINGLE-VALUE)

attributetype (1.1.2.1.1.6 NAME 'x-photo'
 DESC 'foto de la empresa'
 SYNTAX 1.3.6.1.4.1.1466.115.121.1.28)
# ------------------------------------------

objectClass (1.1.2.1.1.1 NAME 'x-desctec'
  DESC 'Descripció tecnica empresa'
  SUP TOP
  STRUCTURAL
  MUST ( x-nom $ x-descripcio )
  MAY ( x-funcionament $ x-document ))

objectClass (1.1.2.2.1.1 NAME 'x-descadd'
  DESC 'Descripció adicional empres'
  SUP TOP
  AUXILIARY
  MUST ( x-direccio $ x-photo ))
```

 

