#! /bin/bash

echo "Configurant el servidor ldap..."
mkdir /var/lib/ldap-a221780it
# 1) Esborrar els directoris de configuració i de dades
rm -rf /var/lib/ldap/*
rm -rf /etc/ldap/slapd.d/*
# 2) Generar el directori de configuració dinàmica slapd.d a partir del fitxer de configuració slapd.conf
slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d
# 3) Injectar a bajo nivel los datos de la BD 'populate' de la organización dc=edt,dc=org
slapadd -F /etc/ldap/slapd.d -l /opt/docker/edt-org.ldif
# 4) Asignar la propiedad y grupo del directorio de datos de configuración al usuario openldap
chown -R openldap:openldap /etc/ldap/slapd.d /var/lib/ldap
# 5) Engegar el servei slapd amb el parametre que fa debug per mantenir-ho engegat en foreground
/usr/sbin/slapd -d0


