### M06-ASO Pràctica (2) LDAP EDITAT

#### Pasos fets:

**1. Creació de fitxes:**
* Dockerfile
```
# servidor ldap 

FROM debian:latest
LABEL author="Iber@edt ASIX-M06"
LABEL subject="ldapserver 2023"
ARG DEBIAN_FRONTEND=noninteractive
RUN apt-get update && apt-get install -y iproute2 nmap vim tree procps less ldap-utils slapd
RUN mkdir /opt/docker
COPY * /opt/docker/
RUN chmod +x /opt/docker/startup.sh
WORKDIR /opt/docker
CMD /opt/docker/startup.sh
EXPOSE 389    
```
* startup.sh
```
#! /bin/bash

# 1) Borra tota la configuració i les dades predetermindes de ldap.
rm -rf /var/lib/ldap/*
rm -rf /etc/ldap/slapd.d/*

# 2) Primer bolco la meva configuració que jo he creat al directori de configuració dinàmica.
slaptest -f /opt/docker/slapd.conf -F /etc/ldap/slapd.d
# 3) Afegeixo les dades del fitxer ldif.
slapadd -F /etc/ldap/slapd.d -l /opt/docker/edt-org.ldif
# 4) Mirar les noves dades.
slapcat

# 5) Assignar la propietat i el grup dels directoris al usuari openldap(client ldap).
chgrp -R openldap.openldap /var/lib/ldap /etc/ldap/slapd.d
# 6) Enxega el dimoni de ldap "slapd".
/usr/sbin/slapd -d0
```
* slapd.conf -> He posat el password de Manager encriptat i he posat una nova base de dades anomenada "config".
```
#
# See slapd.conf(5) for details on configuration options.
# This file should NOT be world readable.
#
# debian packages: slapd ldap-utils

include		/etc/ldap/schema/corba.schema
include		/etc/ldap/schema/core.schema
include		/etc/ldap/schema/cosine.schema
include		/etc/ldap/schema/duaconf.schema
include		/etc/ldap/schema/dyngroup.schema
include		/etc/ldap/schema/inetorgperson.schema
include		/etc/ldap/schema/java.schema
include		/etc/ldap/schema/misc.schema
include		/etc/ldap/schema/nis.schema
include		/etc/ldap/schema/openldap.schema
# include		/etc/ldap/schema/ppolicy.schema
include		/etc/ldap/schema/collective.schema

# Allow LDAPv2 client connections.  This is NOT the default.
allow bind_v2

pidfile		/var/run/slapd/slapd.pid
#argsfile	/var/run/openldap/slapd.args

modulepath /usr/lib/ldap
moduleload back_mdb.la
moduleload back_monitor.la

# ----------------------------------------------------------------------
database mdb
suffix "dc=edt,dc=org"
rootdn "cn=Manager,dc=edt,dc=org"
rootpw {SSHA}WbS2P+U5Huv24E5/AMZUafFvI6MNM8DW 
# rootpw = secret
directory /var/lib/ldap
index objectClass eq,pres
access to * by self write by * read
# ---------------------------------------------------------------------
database config
rootdn "cn=sysadmin,cn=config"
rootpw syskey
# ----------------------------------------------------------------------
database monitor
access to *
       by dn.exact="cn=Manager,dc=edt,dc=org" read
       by * none
```
* edt-org.ldif -> Modifico el fitxer fent que l'“ou=usuaris” tingui 12 usuaris nous que tinguin el nom usuari0 a l'usuari11. També crearé una “ou” nova que jo m’inventaré en la qual dins hi hauran dos usuraris nous (també inventats). Com a última cosa feta, és que en el “dn” dels usuaris, el “cn” es canviarà pel “uid”.
```
dn: dc=edt,dc=org
dc: edt
description: Escola del treball de Barcelona
objectClass: dcObject
objectClass: organization
o: edt.org

dn: ou=maquines,dc=edt,dc=org
ou: maquines
description: Container per a maquines linux
objectclass: organizationalunit

dn: ou=clients,dc=edt,dc=org
ou: clients
description: Container per a clients linux
objectclass: organizationalunit

dn: ou=productes,dc=edt,dc=org
ou: productes
description: Container per a productes linux
objectclass: organizationalunit

dn: ou=usuaris,dc=edt,dc=org
ou: usuaris
description: Container per usuaris del sistema linux
objectclass: organizationalunit

dn: ou=afegits,dc=edt,dc=org
ou: proves
description: Container per fer proves amb usuaris afegits
objectclass: organizationalunit

dn: uid=pau,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: Pau Pou
cn: Pauet Pou
sn: Pou
homephone: 555-222-2220
mail: pau@edt.org
description: Watch out for this guy
ou: Profes
uid: pau
uidNumber: 5000
gidNumber: 100
homeDirectory: /tmp/home/pau
userPassword: {SSHA}NDkipesNQqTFDgGJfyraLz/csZAIlk2/

dn: uid=pere,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: Pere Pou
sn: Pou
homephone: 555-222-2221
mail: pere@edt.org
description: Watch out for this guy
ou: Profes
uid: pere
uidNumber: 5001
gidNumber: 100
homeDirectory: /tmp/home/pere
userPassword: {SSHA}ghmtRL11YtXoUhIP7z6f7nb8RCNadFe+

dn: uid=anna,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: Anna Pou
cn: Anita Pou
sn: Pou
homephone: 555-222-2222
mail: anna@edt.org
description: Watch out for this girl
ou: Alumnes
uid: anna
uidNumber: 5002
gidNumber: 600
homeDirectory: /tmp/home/anna
userPassword: {SSHA}Bm4B3Bu/fuH6Bby9lgxfFAwLYrK0RbOq

dn: uid=marta,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: Marta Mas
sn: Mas
homephone: 555-222-2223
mail: marta@edt.org
description: Watch out for this girl
ou: Alumnes
uid: marta
uidNumber: 5003
gidNumber: 600
homeDirectory: /tmp/home/marta
userPassword: {SSHA}9+1F2f5vcW8z/tmSzYNWdlz5GbDCyoOw

dn: uid=jordi,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: Jordi Mas
cn: Giorgios Mas
sn: Mas
homephone: 555-222-2224
mail: jordi@edt.org
description: Watch out for this girl
ou: Alumnes
ou: Profes
uid: jordi
uidNumber: 5004
gidNumber: 100
homeDirectory: /tmp/home/jordi
userPassword: {SSHA}T5jrMgpJwZZgu0azkLIVoYhiG08/KGUv

dn: uid=admin,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: Administrador Sistema
cn: System Admin
sn: System
homephone: 555-222-2225
mail: anna@edt.org
description: Watch out for this girl
ou: system
ou: admin
uid: admin
uidNumber: 10
gidNumber: 10
homeDirectory: /tmp/home/admin
userPassword: {SSHA}4mS0FycWc5bkpW8/a396SGNDTQUlFSX3

# ----------------------------------------------------------------------------

dn: uid=user01,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: Carlos Matuidi
cn: user Carlos Matuidi cursant 2asix 
sn: alum1
homephone: 555-222-0015
mail: user01@edt.org
description: alumne de 2asix
ou: 2asix
uid: user01
uidNumber: 6001
gidNumber: 600
homeDirectory: /tmp/home/2asix/user01
userPassword: {SHA}YTRlNTg5N2ZiMjYxZTFhN2E4MGFmOThmZmQ3OGRhZTI5MGU5YTQ3ZiAgLQo= 
#(passwd = user01)

dn: uid=user02,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: Manel Torras
cn: user Manel Torras cursant 2asix 
sn: alum2
homephone: 555-222-0016
mail: user02@edt.org
description: alumne de 2asix
ou: 2asix
uid: user02
uidNumber: 6002
gidNumber: 600 
homeDirectory: /tmp/home/2asix/user02
userPassword: {SHA}YmYwYTZlNDViNTM4N2JjMzIwMjE4NzQ5YWIzYWMxYjI5YmRkNTQ0NiAgLQo= 
#(passwd = user02)

dn: uid=user03,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: Xifre el pelos
cn: user Xifre el pelos cursant 2asix 
sn: alum3
homephone: 555-222-0017
mail: user03@edt.org
description: alumne de 2asix
ou: 2asix
uid: user03
uidNumber: 6003
gidNumber: 600 
homeDirectory: /tmp/home/2asix/user03
userPassword: {SHA}ZjU5N2Y4Y2Y2ODY5NzExZmVlMzU1MjE1NTczNGI3NDFkYWM3ZjcxNSAgLQo= 
#(passwd = user03)

dn: uid=user04,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: Jaime
cn: user Jaime cursant 2asix 
sn: alum4
homephone: 555-222-0018
mail: user04@edt.org
description: alumne de 2asix
ou: 2asix
uid: user04
uidNumber: 6004
gidNumber: 600 
homeDirectory: /tmp/home/2asix/user04
userPassword: {SHA}YWY2Mjg5YThjMzE5YzFiNjAzMGNjN2VmYmUxNmQ0YzVkMjljOGM1YiAgLQo= 
#(passwd = user04)

dn: uid=user05,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: Mireia Cano
cn: user Mireia Cano cursant 2asix 
sn: alum5
homephone: 555-222-0019
mail: user05@edt.org
description: alumne de 2asix
ou: 2asix
uid: user05
uidNumber: 6005
gidNumber: 600 
homeDirectory: /tmp/home/2asix/user05
userPassword: {SHA}NWNlMmEwYjA2NjA1ZWU0MGU0MjhhYmYyYTM0MTM3Mzc2OTZjY2RmZCAgLQo= 
#(passwd = user05)

dn: uid=user06,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: Lando Cardisian
cn: user Lando Cardisian cursant 2asix 
sn: alum6
homephone: 555-222-0020
mail: user06@edt.org
description: alumne de 2asix
ou: 2asix
uid: user06
uidNumber: 6006
gidNumber: 600 
homeDirectory: /tmp/home/2asix/user06
userPassword: {SHA}ZGQwZGM2ZGVmMWRiZDZjYWI1OTQxMjhmMThkNjQ5ZGJiMDM5MzBiZCAgLQo= 
#(passwd = user06)

dn: uid=user07,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: Sara 
cn: user Sara cursant 2asix 
sn: alum7
homephone: 555-222-0021
mail: user07@edt.org
description: alumne de 2asix
ou: 2asix
uid: user07
uidNumber: 6007
gidNumber: 600 
homeDirectory: /tmp/home/2asix/user07
userPassword: {SHA}OTY2NGY1OTI3NTg3OWUwNDY2Y2RmYTliMDYzNjAwMzY2MDY2YTdiOCAgLQo= 
#(passwd = user07)

dn: uid=user08,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: Chopin
cn: user Chopin cursant 2asix 
sn: alum8
homephone: 555-222-0022
mail: user08@edt.org
description: alumne de 2asix
ou: 2asix
uid: user08
uidNumber: 6008
gidNumber: 600 
homeDirectory: /tmp/home/2asix/user08
userPassword: {SHA}ZWJlZDc0Nzk1YWM3OTE0ZDY3OTNkZThlY2FmM2YzYjM4MzdmODM5ZSAgLQo= 
#(passwd = user08)

dn: uid=user09,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: Passacaglia
cn: user Passacaglia cursant 2asix 
sn: alum9
homephone: 555-222-0023
mail: user09@edt.org
description: alumne de 2asix
ou: 2asix
uid: user09
uidNumber: 6009
gidNumber: 600 
homeDirectory: /tmp/home/2asix/user09
userPassword: {SHA}NGI0MGRmNzQ5NTRiZjVlMzRjMDg4NGI3YjA3YWI0ZTMyMTIwODVmYiAgLQo= 
#(passwd = user09)

dn: uid=user10,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: Marlen de Dios
cn: user Marlen de Dios cursant 2asix 
sn: alum10
homephone: 555-222-0024
mail: user10@edt.org
description: alumne de 2asix
ou: 2asix
uid: user10
uidNumber: 6010
gidNumber: 600 
homeDirectory: /tmp/home/2asix/user10
userPassword: {SHA}YWZmMWY0ZTZmY2RkZjNlMzI2Y2JlZDNhMjNkNjNmMzEyYTkzMTI2ZSAgLQo= 
#(passwd = user10)

dn: uid=user11,ou=usuaris,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: Abdul Rehmen
cn: user Abdul Rehmen cursant 2asix 
sn: alum11
homephone: 555-222-0025
mail: user11@edt.org
description: alumne de 2asix
ou: 2asix
uid: user11
uidNumber: 6011
gidNumber: 600 
homeDirectory: /tmp/home/2asix/user11
userPassword: {SHA}M2VlNTBiMjIwZmYwMjA0MTMwYWMwNzQ5NTU1NzA4YzcxNTg4MTA4MyAgLQo= 
#(passwd = user11)

# ----------------------------------------------------------------------------

dn: cn=afegit01,ou=afegits,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: afegit01
sn: nou01
homephone: 555-222-0050
mail: afegit01@edt.org
description: Usuari afegit per a la pràctica

dn: cn=afegit02,ou=afegits,dc=edt,dc=org
objectclass: posixAccount
objectclass: inetOrgPerson
cn: afegit02
sn: nou02
homephone: 555-222-0051
mail: afegit02@edt.org
description: Usuari afegit per a la pràctica
```
* fitxer_mod1.ldif -> Modificarà la descripció i el ou del usuari "user10".
```
dn: uid=user10,ou=usuaris,dc=edt,dc=org
changetype: modify
replace: cn
cn: Carlitos
-
delete: description
-
add: description
description: alumne de 1asix
```
* fitxer_mod2.ldif -> Esborrarà la configuració d'accés antiga i afagirà una de nova la qual tothom tindrà accés.
```
dn: olcDatabase={1}mdb,cn=config
changetype: modify
delete: olcAccess
-
add: olcAccess
olcAccess: to * by * read
```
***
#### Comprovacions:
**1. Per posar les contrasenyes encriptades per als usuaris creats:**
```
echo "user01" | shasum | openssl enc -base64
YTRlNTg5N2ZiMjYxZTFhN2E4MGFmOThmZmQ3OGRhZTI5MGU5YTQ3ZiAgLQo=

echo "user02" | shasum | openssl enc -base64
YmYwYTZlNDViNTM4N2JjMzIwMjE4NzQ5YWIzYWMxYjI5YmRkNTQ0NiAgLQo=

echo "user03" | shasum | openssl enc -base64
ZjU5N2Y4Y2Y2ODY5NzExZmVlMzU1MjE1NTczNGI3NDFkYWM3ZjcxNSAgLQo=

echo "user04" | shasum | openssl enc -base64
YWY2Mjg5YThjMzE5YzFiNjAzMGNjN2VmYmUxNmQ0YzVkMjljOGM1YiAgLQo=

echo "user05" | shasum | openssl enc -base64
NWNlMmEwYjA2NjA1ZWU0MGU0MjhhYmYyYTM0MTM3Mzc2OTZjY2RmZCAgLQo=

echo "user06" | shasum | openssl enc -base64
ZGQwZGM2ZGVmMWRiZDZjYWI1OTQxMjhmMThkNjQ5ZGJiMDM5MzBiZCAgLQo=

echo "user07" | shasum | openssl enc -base64
OTY2NGY1OTI3NTg3OWUwNDY2Y2RmYTliMDYzNjAwMzY2MDY2YTdiOCAgLQo=

echo "user08" | shasum | openssl enc -base64
ZWJlZDc0Nzk1YWM3OTE0ZDY3OTNkZThlY2FmM2YzYjM4MzdmODM5ZSAgLQo=

echo "user09" | shasum | openssl enc -base64
NGI0MGRmNzQ5NTRiZjVlMzRjMDg4NGI3YjA3YWI0ZTMyMTIwODVmYiAgLQo=

echo "user10" | shasum | openssl enc -base64
YWZmMWY0ZTZmY2RkZjNlMzI2Y2JlZDNhMjNkNjNmMzEyYTkzMTI2ZSAgLQo=

echo "user11" | shasum | openssl enc -base64
M2VlNTBiMjIwZmYwMjA0MTMwYWMwNzQ5NTU1NzA4YzcxNTg4MTA4MyAgLQo=
```
***
**2. Per aconseguir la contrasenya de Manager encriptada:**
* He creat una nova xarxa.
```
docker network create 2hisx
```
* He ences el container amb la xarxa creada, amb propagació de ports i en mode detach. 
```
docker run --rm -h ldap.edt.org --name ldap --net 2hisx -p 389:389 -d ldap23:editat
```
* He entrat fent l'ordre per poder encriptar la contrasenya de Manager. 
```
docker exec -it ldap slappasswd 
New password: secret
Re-enter new password: secret
{SSHA}WbS2P+U5Huv24E5/AMZUafFvI6MNM8DW
```
***
**3. Exemples verificació  de modificar configuració base de dades:**
*  Creació de l'imatge.
```
docker build -t ldap23:editat .
```
* Iniciar el container (mode detach).
```
docker run --rm -h ldap.edt.org --name ldap --net 2hisx -p 389:389 -d ldap23:editat
```
* Entrar interactivament al container.
```
docker exec -it ldap /bin/bash
```
* Fer la modificació dels permisos d'accéss de la base de dades a traves del fitxer "fitxer_mod2.ldif. Ens connectarem com al administrador de la base de dades **config** i amb la seva contrasenya corresponent.
```
ldapmodify -vx -D 'cn=sysadmin,cn=config' -w syskey -f fitxer_mod2.ldif 
ldap_initialize( <DEFAULT> )
delete olcAccess:
add olcAccess:
	to * by * read
modifying entry "olcDatabase={1}mdb,cn=config"
modify complete
```
* Comprovació de que s'ha fet bé:
```
ldapsearch -x -LLL -D 'cn=sysadmin,cn=config' -w syskey -b 'olcDatabase={1}mdb,cn=config' olcAccess

dn: olcDatabase={1}mdb,cn=config
olcAccess: {0}to * by * read
```
* Fer la modificació amb el fitxer "fitxer_mod1.ldif". Hem dona error.
```
ldapmodify -cvx -D 'uid=user10,ou=usuaris,dc=edt,dc=org' -w user10 -f fitxer_mod1.ldif 
ldap_initialize( <DEFAULT> )
ldap_bind: Invalid credentials (49)
```

